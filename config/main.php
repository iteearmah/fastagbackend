<?php
return [
    'promo_domain' => env('PROMO_DOMAIN', 'promo.fastag.dv'),
    'client_domain' => env('CLIENT_DOMAIN', 'client.fastag.dv'),
    'selfcare_domain' => env('SELF_CARE_DOMAIN', 'register.fastag.dv'),
    'selfcare_password' => env('SELF_CARE_PASSWORD', '123456'),
    'server_password' => env('SERVER_PASSWORD'),
    'organizer_logo_dir' => 'photos/organizer-logo',
    'cover_photo_dir' => 'photos/cover-photo',
    'qr_code_dir' => 'photos/qr-code',
    'print_dir' => env('PRINT_DIRECTORY'),
    'programme_dir' => 'programme',
    'admin_role' => 'Admin',
    'event_sms' => 'Welcome to the [EVENT_TITLE]. Click [EVENT_LINK] to access the program. PIN Code: [ATTENDEE_PIN_CODE] By Fastag',
    'lived_abroad_options' => ['No', 'Yes', 'Prefer not to say'],
];
