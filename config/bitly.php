<?php
/*
* (c) Wessel Strengholt <wessel.strengholt@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

return [
    /*
    |--------------------------------------------------------------------------
    | Access Token
    |--------------------------------------------------------------------------
    |
    | Enter here your access token generated from: https://bitly.com/a/oauth_apps
    */

    'accesstoken' => env('BITLY_ACCESS_TOKEN', '4cad38b61d6d0c835daec389db13eccb25fe21e7'),
];
