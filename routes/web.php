<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['domain' => config('main.selfcare_domain')], function ()
{
    Route::get('/', 'SelfServiceEventController@home')->name('selfcare.event-home');
    Route::get('/{event_code}', 'SelfServiceEventController@index')->name('selfcare.event-form');
});

Route::group(['domain' => config('main.promo_domain')], function ()
{
    Route::get('/', 'PromoPageController@eventNotFound');
    Route::get('/{event_uuid}', 'PromoPageController@index')->name('promopage');
    Route::get('/e/{event_code}', 'PromoPageController@shortLinkRedirect')->name('promopageredirect');
});

Route::get('/', 'HomeController@index')->name('home');
Auth::routes();
Route::group(['domain' => config('main.client_domain'), 'middleware' => ['auth', 'role:Organizer']], function ()
{
    Route::get('/dashboard', 'ClientDashboardController@index')->name('client.dashboard');
    Route::get('/agreement', 'ClientAgreementController@index')->name('client.agreement');
    Route::post('/save-agreement', 'ClientAgreementController@store')->name('client.agreementstore');
    Route::get('/events', 'ClientEventController@index')->name('clientevents.index');
    Route::get('/{uuid}/attendees', 'ClientEventController@attendees')->name('clientevents.attendees');
});
Route::group(['middleware' => ['auth', 'role:Admin']], function ()
{
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('organizers', 'OrganizerController');
    Route::resource('events', 'EventController');
    Route::get('/events/{id}/sms', 'EventController@sms')->name('events.sms');
    Route::post('/events/{id}/sendsms', 'EventController@sendsms')->name('events.sendsms');
    Route::get('/events/{id}/attendees-clockedin', 'EventController@attendees_clockedin')->name('events.attendeesclockedin');
    Route::get('/attendees/import', 'AttendeeController@import')->name('attendees.import');
    Route::post('/attendees/storeimport', 'AttendeeController@storeimport')->name('attendees.storeimport');
    Route::get('/eventguest/import', 'EventGuestController@import')->name('eventguest.import');
    Route::resource('eventguest', 'EventGuestController');
    Route::post('/eventguest/storeimport', 'EventGuestController@storeimport')->name('eventguest.storeimport');
    Route::get('/attendees/export/{event_id}', 'AttendeeController@export')->name('attendees.export');
    Route::get('/attendees/clearall/{event_id}', 'AttendeeController@clearall')->name('attendees.clearall');
    Route::resource('attendees', 'AttendeeController');

});
Route::get('/selfcare/{event_code}', 'SelfServiceEventController@index')->name('selfcare.event-form');