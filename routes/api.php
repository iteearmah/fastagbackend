<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
//Route::get('attendees-search/{event_id}', 'API\AttendeeController@attendeesSearch');

/*use App\EventGuest;
use App\Http\Resources\EventGuestCollection;

Route::get('/testdata', function (Request $request)
{
    $eventGuests = EventGuest::all();

    return new EventGuestCollection($eventGuests);
});*/

Route::get('testdata/{type}', 'API\EventGuestController@getAllGuest');

Route::get('/test', function (Request $request)
{
    //$class = new \App\Http\Controllers\API\AttendeeController();
    ///$return = $class->attendeePrintTag('2774543c-68df-470a-a454-5c3dca657a1a');
    //print_r($return);
    // EventService::removeAttendanceCache('nl8G34');
    $num = rand(1, 999);
     Cache::put('number_event_attendees_nl8G34', $num, 30);
    echo "number_event_attendees_nl8G34: " . Cache::get('number_event_attendees_nl8G34');

});

Route::middleware('auth:api')->get('/user', function (Request $request)
{
    return $request->user();
});
Route::post('login', 'API\PassportController@login');

Route::group(['middleware' => ['auth:api'], 'namespace' => 'API'], function ()
{
    //Route::post('logout', 'PassportController@logout');
    Route::resource('events', 'EventController');
    Route::resource('attendees', 'AttendeeController');
    Route::get('attendees-clockedin/{event_id}', 'AttendeeController@attendees_clockedin');
    Route::post('attendees-search/{event_id}', 'AttendeeController@attendeesSearch');
    Route::post('attendee-printag', 'AttendeeController@attendeePrintTag');
});
