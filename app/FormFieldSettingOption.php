<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Yajra\Auditable\AuditableTrait;

class FormFieldSettingOption extends Model
{
    use AuditableTrait;
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $fillable = ['form_field_setting_id','label','value'];
    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->id = Uuid::uuid4()->toString();
        });
    }
}
