<?php

namespace App\Contracts\Services;

interface IDataSyncJobService
{
    public function pull();

    public function push();
}