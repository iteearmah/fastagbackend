<?php

namespace App\Jobs;

use App\Contracts\Services\IDataSyncJobService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class DataSyncJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var mixed|string
     */
    private $action;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($action = 'pull')
    {
        $this->action  = $action;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(IDataSyncJobService $dataSyncJobService)
    {
         $dataSyncJobService->{$this->action}();
    }
}
