<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;
use Emadadly\LaravelUuid\Uuids;
class Organizer extends Model
{
    use SoftDeletes, AuditableTrait, Uuids;
    protected $table    = 'organizers';
    protected $fillable = ['name', 'email', 'phone', 'status', 'website', 'address', 'logo_filename'];
}
