<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventGuestAttendee extends Model
{
    protected $fillable = ['attendee_id', 'event_guest_id'];

    public function event_guest()
    {
        return $this->belongsTo('App\EventGuest', 'event_guest_id', 'id');
    }

    public function attendee()
    {
        return $this->belongsTo('App\Attendee', 'attendee_id', 'id');
    }
}
