<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AttendeeClockedIn
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $attendee_event;

    /**
     * Create a new event instance.
     *
     * @param collection $event attendee record
     * @return void
     */
    public function __construct($attendee_event)
    {
        $this->attendee_event = $attendee_event;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
