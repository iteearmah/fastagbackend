<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PrintTag
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $pin_code;
    public $full_name;
    public $company;
    public $organizer_logo;
    public $tag_filename;
    public $otherData;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($pin_code = 'test', $full_name = null, $company = null, $organizer_logo = null, $tag_filename = '', $otherData = [])
    {
        $this->pin_code = $pin_code;
        $this->full_name = $full_name;
        $this->company = $company;
        $this->organizer_logo = $organizer_logo;
        $this->tag_filename = $tag_filename;
        $this->otherData = $otherData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
