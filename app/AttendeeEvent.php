<?php

namespace App;

use App\Attendee;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class AttendeeEvent extends Model
{
    protected $table    = 'attendee_events';
    protected $fillable = ['event_id', 'attendee_id', 'pin_code'];
    public $incrementing = false;
    protected $primaryKey = 'id';

    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->id = Uuid::uuid4()->toString();
        });
    }

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id', 'id');
    }

    public function attendee()
    {
        return $this->belongsTo('App\Attendee', 'attendee_id', 'id');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance', 'event_id', 'event_id');
    }

    public function scopeEventID($query, $event_id = null)
    {
        $query->where('event_id', $event_id ?: $this->event_id);
    }

    public function scopeAttendeeID($query, $attendee_id = null)
    {
        $query->where('attendee_id', $attendee_id ?: $this->attendee_id);
    }

    public function scopePinCode($query, $pin_code = null)
    {
        $query->where('pin_code', $pin_code ?: $this->pin_code);
    }



}
