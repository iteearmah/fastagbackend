<?php

namespace App\Services;

use App\Attendee;
use Excel;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class AttendeeService
{
    public static function generateQRCode($event, $attendee, $pin_code)
    {
        $qrCode = new BaconQrCodeGenerator;
        $qrCode->format('png')
            ->size(200)
            /*->merge(public_path('assets/images/logo-circle.png'), .3, true)*/
            ->generate($attendee->id, public_path(config('main.qr_code_dir') . '/' . $pin_code .'.png'));
    }

    public static function setAttendeeRecords($request, $attendee)
    {
        $attendee->event = $request->input('event');
        $attendee->full_name = $request->input('full_name');
        $attendee->phone = $request->input('phone');
        $attendee->email = $request->input('email');
        $attendee->company = $request->input('company');
        $attendee->position = $request->input('position');
        $attendee->gender = $request->input('gender');
        $attendee->table_number = $request->input('table_number');
        $attendee->age = $request->input('age');
        $attendee->level_education = $request->input('level_education');
        $attendee->location = $request->input('location');
        $attendee->lived_abroad = $request->input('lived_abroad');
        $attendee->attending_as = $request->input('attending_as');
        $attendee->mode = $request->input('mode');
        $attendee->registration = $request->input('registration');
        $attendee->place_from = $request->input('place_from');
        $attendee->degree_type = $request->input('degree_type');
        $attendee->certificate_type = $request->input('certificate_type');
        $attendee->interest_area = $request->input('interest_area');
        $attendee->own_business_entrepreneur = $request->input('own_business_entrepreneur');
        $attendee->lived_abroad_country = $request->input('lived_abroad_country');
        $attendee->lived_abroad_period = $request->input('lived_abroad_period');
        $attendee->find_out_event = $request->input('find_out_event');
        $attendee->guest_represent = $request->input('guest_represent');
        $attendee->country = $request->input('country');

        return $attendee;
    }

    public static function export($request, $event_id)
    {
        if ($event_id)
        {
            $attendees = Attendee::with('Event', 'Attendance')->whereEvent($event_id)->filter($request)->get();

            return Excel::create('attendees_data', function ($excel) use ($attendees)
            {
                $excel->sheet('attendees', function ($sheet) use ($attendees)
                {

                    if (!empty($attendees) && $attendees->count())
                    {
                        foreach ($attendees as $attendee)
                        {
                            if (!empty($attendee))
                            {
                                $dataArray[] =
                                    [
                                        'Pin Code' => $attendee['attendee_event']->pin_code ?? '',
                                        'Full Name' => $attendee['full_name'] ?? '',
                                        'Phone' => $attendee['phone'] ?? '',
                                        'Email' => $attendee['email'] ?? '',
                                        'Company' => $attendee['company'] ?? '',
                                        'Position' => $attendee['position'] ?? '',
                                        'Gender' => $attendee['gender'] ?? '',
                                        'Age' => $attendee['age'] ?? '',
                                        'Location' => $attendee['location'] ?? '',
                                        'Country' => $attendee['country'] ?? '',
                                        'Level of Education' => $attendee['level_education'] ?? '',
                                        'Lived Abroad' => $attendee['lived_abroad'] ?? '',
                                        'Attending As' => $attendee['attending_as'] ?? '',
                                    ];
                            }
                        }
                        $sheet->fromArray($dataArray);
                    }

                });
            })->download('xlsx');
        }
    }
}
