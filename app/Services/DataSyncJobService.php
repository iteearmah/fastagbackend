<?php

namespace App\Services;


use App\Contracts\Services\IDataSyncJobService;

class DataSyncJobService implements IDataSyncJobService
{
    public function pull()
    {
        return 'qpull';
    }

    public function push()
    {
        return 'vpush';
    }
}
