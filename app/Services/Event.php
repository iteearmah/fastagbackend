<?php

namespace App\Services;

use App\Attendance;
use App\Attendee;
use App\AttendeeEvent;
use App\Event as EventModel;
use App\PrintField;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Shivella\Bitly\Exceptions\InvalidResponseException;
use Shivella\Bitly\Facade\Bitly;

class Event
{

    public function generateCode()
    {
        $eventCodes = EventModel::pluck('event_code')->toArray(); // Generate a new unique number
        do
        {
            $eventCode = rand(1000, 9999);
        } while (in_array($eventCode, $eventCodes));

        return $eventCode;
    }
    public static function eventCodeToID($event_code = null)
    {
        if ($event_code)
        {
            return EventModel::where('event_code', '=', $event_code)->pluck('id')->first();
        }
        return null;
    }

    /**
     * Get the numbers days of an Event.
     * @param  int  $event_id
     * @return integers
     */

    public static function eventDays($event_id = null)
    {
        return Attendance::eventdays($event_id)->pluck('clocked_in_date');
    }

    public static function atteedee_attendance_days($event_id)
    {
        $attendees            = Attendee::with('Event', 'attendee_event','Attendance')->where('event', '=', $event_id)
            ->whereHas('attendee_event', function ($q) {
            $q->orderBy('created_at', 'desc');
        })->get();
        $attendee_attendances = [];
        if ($attendees)
        {
            $attendee_attendance = [];
            foreach ($attendees as $attendee)
            {
                if ($attendee->Attendance)
                {
                    foreach ($attendee->Attendance as $attendance)
                    {
                        $attendee_attendances[$attendee->id][$attendance->clocked_in_date->format('Y-m-d')] = $attendance->clocked_in_date->format('d-m-Y'). " <br> " .\Carbon\Carbon::parse($attendance->clocked_in_time)->format('H:i');
                    }

                }
            }
        }
        return $attendee_attendances;
    }

    public static function event_days($event_id)
    {
        $eventdays  = self::eventDays($event_id);
        $event_days = [];
        if ($eventdays)
        {
            foreach ($eventdays as $event_day)
            {
                $event_days[$event_day->format('Y-m-d')] = $event_day;
            }
        }
        return $event_days;
    }

    public static function attendance($event_id)
    {
        $attendees = AttendeeEvent::with(['attendee' => function ($query)
        {
            $query->orderBy('attendees.full_name', 'asc');
        }], 'event')->eventid($event_id);
        /*  $allEventAttendanceID = Attendance::distinct('attendee_id')->eventid($event_id)->pluck('attendee_id')->all();
        $attendees            = Attendee::whereIn('id', $allEventAttendanceID)->with('event', 'attendee_event');*/
        return $attendees;
    }

    public static function setAttendanceCache($event_id, $event_code)
    {
        if ($event_id && $event_code)
        {
            $totalNum = Attendee::eventattendee($event_id)->where('mode', '=', 'live')->has('Attendance')->count();
            Cache::forever('number_event_attendees_' . $event_code, $totalNum);
        }
    }
    public static function getAttendanceCache($event_id, $event_code)
    {
        $memKey   = 'number_event_attendees_' . $event_code;
        $totalNum = Cache::get($memKey);
        if ($event_id && $event_code && $totalNum)
        {
            $totalNum = Attendee::eventattendee($event_id)->where('mode', '=', 'live')->has('Attendance')->count();
            Cache::forever($memKey, $totalNum);
        }
        return $totalNum;
    }

    public static function removeAttendanceCache($event_code)
    {
        if ($event_code)
        {
            Cache::forget('number_event_attendees_' . $event_code);
        }
    }

    public static function savePrintFields($event_id, $selectedPrintFields)
    {
        if($selectedPrintFields)
        {
            $printFields = PrintField::FIELDS;
            $selectedPrintFields = array_keys($selectedPrintFields);
            $data = ['event_id' => $event_id];
            foreach ($printFields as $key => $printField)
            {
                $switch = in_array($key, $selectedPrintFields) ? 1 : 0;
                $data = array_merge($data, [$key => $switch]);
            }
            PrintField::updateOrCreate(['event_id' => $event_id], $data);
        }
    }

    public static function generateShortUrl($event_code)
    {
        if($event_code)
        {
            try {
                $originalUrl = route('selfcare.event-form', ['event_code' => $event_code]);
                $shortUrl = Bitly::getUrl($originalUrl);
                if($shortUrl)
                {
                    EventModel::whereEventCode($event_code)->update(['short_url' => $shortUrl]);
                }
            }catch (InvalidResponseException $exception)
            {
                Log::error($exception->getMessage());
            }

        }
    }

}
