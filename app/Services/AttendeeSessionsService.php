<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class AttendeeSessionsService
{
    public static function eventAttendeeSessions($eventID)
    {
        return DB::select(DB::raw("SELECT
                        atd.id AS id,
                        atd.full_name AS attendee,
                        atd.phone,
                        atd.email,
                        atd.company,
                        atd.position,
                        ess.`name` AS session_name,
                        ess.event_id AS event_id,
                        ( SELECT `events`.title FROM `events` WHERE `events`.id = ess.event_id COLLATE utf8mb4_general_ci ) AS event_name,
                        att.created_at
                        FROM
                            event_session_attendance att
                            JOIN event_sessions ess ON att.session_id = ess.id
                            JOIN attendees atd ON (att.attendee_id = atd.id COLLATE utf8mb4_general_ci)
                            WHERE ess.event_id = '$eventID'
                            ORDER BY att.created_at DESC"));
    }
}
