<?php

namespace App\Services;

use App\AttendeeEvent;

class PinCode
{
    /**
     * Generate Pin Code.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function generate($id)
    {
        $ids = AttendeeEvent::where('event_id', '=', $id)->pluck('pin_code')->toArray(); // Generate a new unique number
        do
        {
            $id = rand(1000, 9999);
        } while (in_array($id, $ids));

        return $id;
    }

    public function verifyAttendeeEvent($pin_code = null, $event_id = null)
    {
        $attendee_event = AttendeeEvent::with('event')->eventid($event_id)->pincode($pin_code)->first();
        return ($attendee_event) ? $attendee_event : false;
    }
}
