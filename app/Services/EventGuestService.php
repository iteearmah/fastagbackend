<?php

namespace App\Services;

use App\EventGuestAttendee;
use App\EventGuest;
use App\Event as EventModel;


class EventGuestService
{

    public function tableSeatsFull($eventguest_id)
    {
        if($eventguest_id)
        {
            $eventGuestAttendee = EventGuestAttendee::with('event_guest')->find($eventguest_id);
            if($eventGuestAttendee)
            {
                $eventguest = $eventGuestAttendee->event_guest;

                return ($eventguest->seats_on_table <= $eventGuestAttendee->count()) ? true : false;
            }
        }

        return false;
    }


}
