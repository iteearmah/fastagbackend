<?php

namespace App;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\Auditable\AuditableTrait;

class EventGuest extends Model
{
    use SoftDeletes, AuditableTrait, Uuids;
    protected $fillable = ['event_id', 'name', 'table_number', 'seats_on_table', 'type'];

    public function event_guest_attendees()
    {
        return $this->hasMany('App\EventGuestAttendee', 'event_guest_id', 'id');
    }

    public function Event()
    {
        return $this->belongsTo('App\Event', 'event_id', 'id');
    }

    public function scopeType($query, $type)
    {
        if($type)
        {
            $query->where('type', '=', $type);
        }

    }

    public function scopeFilter($query, $request)
    {
        if ($request->filled('event'))
        {
            $event_id = $request->input('event');
            $query->where('event_id', '=', $event_id);
        }

        if ($request->has('search') && $request->filled('search'))
        {
            $search = $request->input('search');
            $terms  = explode(' ', $search);
            $query->where(function ($query) use ($terms)
            {
                if ($terms)
                {
                    foreach ($terms as $term)
                    {
                        $query->orWhere('name', 'like', '%' . $term . '%');
                    }
                }

            });
        }
        return $query;
    }
}
