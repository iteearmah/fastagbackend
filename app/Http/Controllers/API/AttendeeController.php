<?php

namespace App\Http\Controllers\API;

use App\Attendance;
use App\Attendee;
use App\AttendeeEvent;
use App\Event;
use App\EventGuest;
use App\EventGuestAttendee;
use App\Events\AttendeeClockedIn;
use App\Events\PrintTag;
use App\Http\Controllers\Controller;
use App\Http\Resources\AttendeeCollection;
use App\Services\AttendeeService;
use App\Services\Event as EventService;
use App\Services\PinCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use Storage;
use Validator;
use Log;
class AttendeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @param  $pinCode
     */
    public function store(PinCode $pinCode, Request $request)
    {
        $printNewTag = true;
        $otherData = [];
        $otherData['table_number'] = null;
        $otherData['attendee'] = null;
        $request = $request->merge($this->fixNull($request->input()));
        Log::info(print_r($request->input(), true));
        $registration = $request->input('registration');
        $otherData['location'] = '';

        $otherData['location_to_country'] = ($request->has('location_to_country'))? $request->input('location_to_country') : 'off';
        if ($registration == 'pin')
        {
            $validator = Validator::make($request->all(), [
                'pin_code' => 'required|max:4',
            ]);
        }
        else
        {
            if ($request->input('guest_show') == 'off')
            {
                $validator = Validator::make($request->all(), [
                    'full_name' => 'required|min:2|max:255',
                    /*'phone' => 'required|regex:/[0-9]{10,15}/|digits_between:10,15',*/
                ]);
            }
        }

        if ($request->input('guest_show') == 'off')
        {
            if ($validator->fails())
            {
                return response()->json([
                    'status' => 'error',
                    'message' => $validator->errors()->all(),
                ], 400);
            }
        }
        Log::debug(print_r($request->input(), true));
        //Verify pin from app
        if ($registration == 'pin')
        {
            $pin_code = $request->input('pin_code');
            $event_code = $request->input('event_code');
            $event = Event::where('event_code', '=', $event_code)->first();
            $attendeeEvent = $pinCode->verifyAttendeeEvent($pin_code, $event->id);
            if (!$attendeeEvent)
            {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Invalid Pin',
                ], 400);
            }
            $event = ($attendeeEvent->event) ? $attendeeEvent->event : null;
            $attendee = ($attendeeEvent->attendee) ? $attendeeEvent->attendee : null;
            if ($request->has('lived_abroad') && $request->filled('lived_abroad'))
            {
                $attendee->lived_abroad = $request->input('lived_abroad');
                $attendee->save();
            }

            if( $event && $event->tag_image_option == 'qr_code') {
                AttendeeService::generateQRCode($event, $attendee, $pin_code);
            }
        }
        else
        {
            $event_code = $request->input('event_code');
            $event = Event::where('event_code', '=', $event_code)->first();
            $pin_code = $pinCode->generate($event->id);
            $attendee = Attendee::where('email', '=', $request->input('email'))->where('phone', '=', $request->input('phone'))->where('full_name', '=', $request->input('full_name'))->where('company', '=', $request->input('company'))->where('gender', '=', $request->input('gender'))->where('table_number', '=', $request->input('table_number'))->where('mode', '=', $request->input('mode'))->where('country', '=', $request->input('country'))->where('position', '=', $request->input('position'))->where('age', '=', $request->input('age'))->where('level_education', '=', $request->input('level_education'))->where('location', '=', $request->input('location'))->where('attending_as', '=', $request->input('attending_as'))->where('lived_abroad', '=', $request->input('lived_abroad'))->whereDate('created_at', Carbon::today())->orderBy('created_at', 'DESC')->first();
        }

        $attendee_id = $attendee->id ?? null;
        $attendance = Attendance::where('event_id', '=', $event->id)->where('attendee_id', '=', $attendee_id)->whereDate('clocked_in_date', '=', Carbon::now()->format('Y-m-d'))->first();
        if (!$attendance)
        {
            $printNewTag = isset($attendee->Attendance) && $attendee->Attendance->count() == 0;
            if (!$attendee_id)
            {
                $attendee = new Attendee();
                $attendee->event = $event->id;
                $request->request->add(['event' => $event->id]);
                //Log::info(print_r($request->input(),true));
                $attendee = AttendeeService::setAttendeeRecords($request, $attendee);
                //Log::info(print_r($request->input(),true));
                if ($attendee->save())
                {
                    if( $event && $event->tag_image_option == 'qr_code') {
                        AttendeeService::generateQRCode($event, $attendee, $pin_code);
                    }
                    AttendeeEvent::Create(['event_id' => $attendee->event, 'attendee_id' => $attendee->id, 'pin_code' => $pin_code]);
                    $attendee_id = $attendee->id;
                }
            }

            if ($attendee_id)
            {
                $attendance = Attendance::Create(['event_id' => $event->id, 'attendee_id' => $attendee_id, 'clocked_in_date' => Carbon::now(), 'clocked_in_time' => Carbon::now(), 'mode' => $request->input('mode')]);
                if ($request->input('guest_show') == 'on')
                {
                    EventGuestAttendee::Create(['event_guest_id' => $request->input('guest_selected'), 'attendee_id' => $attendee_id]);
                }
            }
            $attendee_event = AttendeeEvent::with('event', 'attendee')->attendeeid($attendee_id)->first();

            //TODO put organizer logo in separate service
            $organizerLogo = (isset($event->Organizer->logo_filename)) ? asset(config('main.organizer_logo_dir') . '/medium.' . $event->Organizer->logo_filename) : '';
            $organizerLogo = (isset($event->cover_photo)) ? asset(config('main.cover_photo_dir') . '/medium.' . $event->cover_photo) : $organizerLogo;
                //Log::debug("organizerLogo: " . $organizerLogo);
            $otherData['attendee'] = $attendee;
            $otherData['event_obj'] = $event;
            if ($request->has('table_number') && $request->filled('table_number'))
            {
                $otherData['table_number'] = $request->input('table_number');
            }
            $otherData['location'] = $attendee->location;
            $otherData['event'] = $attendee->Event;
            $otherData['pin_code'] = $attendee->attendee_event->pin_code;


            if ($registration == 'pin')
            {
                if ($printNewTag)
                {
                    //Log::info(print_r($otherData['location_to_country'],true));
                    //Log::info(print_r($otherData['location'],true));
                    //dd($otherData['location']);
                    $this->printTag($attendee->attendee_event->pin_code, $attendee->full_name, $attendee->company, $organizerLogo, $attendee->attendee_event->pin_code, $otherData);
                }
            }
            else
            {

                if ($request->input('guest_show') == 'on')
                {
                    $otherData['guest_show'] = $request->input('guest_show');
                    $eventGuest = EventGuest::find($request->input('guest_selected'));
                    //Log::debug(print_r($eventGuest, true));
                    if ($eventGuest)
                    {
                        $otherData['table_number'] = $eventGuest['table_number'];
                    }
                    $this->printTag($attendee->attendee_event->pin_code, $attendee->full_name, $attendee->company, $organizerLogo, $attendee->attendee_event->pin_code, $otherData);
                    $this->printTag($attendee->attendee_event->pin_code, $attendee->full_name, $attendee->company, $organizerLogo, $attendee->attendee_event->pin_code . '.2', $otherData);
                }
                else
                {
                    $this->printTag($attendee->attendee_event->pin_code, $attendee->full_name, $attendee->company, $organizerLogo, $attendee->attendee_event->pin_code, $otherData);
                }

            }

            event(new AttendeeClockedIn($attendee_event));
            EventService::setAttendanceCache($event->id, $event->event_code);
            if ($registration == 'pin')
            {
                $attendee->mode = $request->input('mode');
                $attendee->save();
                return response()->json([
                    'status' => 'success',
                    'full_name' => $attendee->full_name,
                    'message' => 'Attendee record saved',
                ], 200);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Attendee record saved',
            ], 200);
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'User already registered for the day',
            ], 400);
        }

    }

    private function fixNull($data)
    {
        return collect($data)->map(function ($value, $key) {
            if ($value == 'null') {
                return null;
            } else {
                return $value;
            }
        })->all();
    }


    /**
     * @param string $pin_code
     * @param null $full_name
     * @param null $company
     */
    public function printTag($pin_code = 'test', $full_name = null, $company = null, $organizer_logo = null, $tag_filename = '', $otherData = [])
    {
        event(new PrintTag($pin_code, $full_name, $company, $organizer_logo, $tag_filename, $otherData));
    }

    public function attendeesSearch($event_code = null, Request $request)
    {
        if ($request->filled('search'))
        {
            $search = $request->input('search');
            $event_id = EventService::eventCodeToID($event_code);
            $attendees = Attendee::filter($request)->eventattendee($event_id)->has('Attendance')->get();
            if (isset($attendees) && $attendees->count() > 0)
            {
                return new AttendeeCollection($attendees);
            }
        }
        return response()->json([
            'status' => 'error',
            'message' => 'No results found',
            'data' => null,
        ], 400);
    }

    public function attendees_clockedin($event_code = null)
    {
        $totalNum = EventService::getAttendanceCache(EventService::eventCodeToID($event_code), $event_code);
        return response()->json([
            'status' => 'success',
            'message' => 'Event attendees count refreshed!',
            'data' => $totalNum,
        ], 200);
    }

    public function attendeePrintTag(Request $request)
    {
        $uuid = $request->input('uuid');
        if ($uuid)
        {
            $attendee = Attendee::with('attendee_event')->uuid($uuid);
            if ($attendee)
            {
                $otherData['table_number'] = $attendee['table_number'];
                $otherData['event'] = $attendee->Event;
                $otherData['pin_code'] = $attendee->attendee_event->pin_code;


                $organizerLogo = (isset($attendee->Event->Organizer->logo_filename)) ? asset(config('main.organizer_logo_dir') . '/medium.' . $attendee->Event->Organizer->logo_filename) : '';
                $organizerLogo = (isset($attendee->Event->cover_photo)) ? asset(config('main.cover_photo_dir') . '/medium.' . $attendee->Event->cover_photo) : $organizerLogo;
                $this->printTag($attendee->attendee_event->pin_code, $attendee->full_name, $attendee->company, $organizerLogo, $attendee->attendee_event->pin_code, $otherData);
                return response()->json([
                    'status' => 'success',
                    'message' => 'Attendee tag generated successfully!',
                    'data' => null,
                ], 200);
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Attendee not found!',
                    'data' => null,
                ], 400);
            }
        }
        else
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Provide attendance uuid',
                'data' => null,
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
