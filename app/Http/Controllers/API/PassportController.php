<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class PassportController extends Controller
{

    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {

        //\Log::info(print_r(['phone' => request('phone'), 'password' => request('password')], true));

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')]))
        {
            $user              = Auth::user();
            $createToken       = $user->createToken($user->name, ['save-attendee']);
            $token             = $createToken->token;
            $token->expires_at = \Carbon\Carbon::now()->addHour(24);
            $token->save();
            $success['token']     = $createToken->accessToken;
            $success['user_uuid'] = $user->uuid;
            //$success['createToken'] = $token;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else
        {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'       => 'required',
            'phone'      => 'required',
            'password'   => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input             = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user              = User::create($input);
        $success['token']  = $user->createToken(config('app.name'))->accessToken;
        $success['name']   = $user->name;

        return response()->json(['success' => $success], $this->successStatus);
    }

    /**
     * Logout api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = $request->user();
        if ($user->token())
        {
            $user->token()->delete();
            return response()->json(['success' => 'You are Logged out.'], $this->successStatus);
        }
        else
        {
            return response()->json(['error' => 'No active user session was found'], '404');
        }

    }

    /**
     * refresh api
     *
     * @return \Illuminate\Http\Response
     */
    public function refresh(Request $request)
    {
        $token = $user->token();
        if ($token->expires_at > \Carbon\Carbon::now())
        {
            $token->expires_at = \Carbon\Carbon::now()->addMinute(10);
            $token->save();
        }
        return response()->json(['success' => $user], $this->successStatus);
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function getDetails()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
