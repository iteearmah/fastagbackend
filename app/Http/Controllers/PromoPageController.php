<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

class PromoPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($event_uuid)
    {
        $event = Event::uuid($event_uuid);
        if (!$event)
        {
            $this->eventNotFound();
        }
        return view('promo.index', compact('event'));
    }

    public function shortLinkRedirect($event_code, Request $request)
    {
        if ($event_code)
        {
            $event_uuid = Event::where('event_code', '=', $event_code)->pluck('uuid')->first();
            return redirect()->route('promopage', [$event_uuid]);
        }
        else
        {
            $this->eventNotFound();
        }
    }

    public function eventNotFound()
    {

    }

}
