<?php

namespace App\Http\Controllers;

use App\Attendee;
use App\AttendeeEvent;
use App\Event;
use App\Services\AttendeeService;
use App\Services\PinCode;
use Excel;
use Illuminate\Http\Request;

class AttendeeController extends Controller
{

    protected $formRules;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->formRules = [
            'event' => 'required',
            'full_name' => 'required|min:2|max:255',
            'phone' => 'sometimes|nullable|regex:/[0-9]{10,15}/|digits_between:10,15',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $event = null;
        if ($request->filled('event'))
        {
            $event = Event::find($request->input('event'));
            $attendees = Attendee::with('Event', 'Attendance')->filter($request)->orderBy('created_at','DESC')->get();
        }
        else
        {
            $attendees = Attendee::with('Event', 'Attendance')->filter($request)->orderBy('created_at','DESC')->paginate(10)->appends(request()->query());
        }

        return view('attendees.index', compact('attendees', 'event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $event_id = $request->get('event');
        $pinCodeGen = new PinCode();
        $pin_code = $pinCodeGen->generate($event_id);
        $events = Event::all();
        $attendee = Attendee::find(null);
        return view('attendees.create', compact('events', 'event_id', 'pin_code','attendee'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->formRules);
        $attendee = new Attendee();
        $attendee = AttendeeService::setAttendeeRecords($request, $attendee);

        if ($attendee->save())
        {
            AttendeeEvent::Create(['event_id' => $attendee->event, 'attendee_id' => $attendee->id, 'pin_code' => $request->input('pin_code')]);
            $attendee->refresh();
            AttendeeService::generateQRCode($attendee->Event, $attendee, $request->input('pin_code'));
            flash('Attendee was successfully added!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('attendees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attendee = Attendee::with('Event', 'Attendance')->findOrfail($id);
        if (!$attendee)
        {
            abort('404');
        }
        return view('attendees.show', compact('attendee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pin_code = null;
        $attendee = Attendee::with('Event', 'attendee_event')->findOrfail($id);
        if (!$attendee)
        {
            abort('404');
        }
        $event_id = $attendee->event;
        $pin_code = $attendee->attendee_event->pin_code;
        $event = $attendee->Event;
        $events = Event::all();
        $lived_abroad_options = config('main.lived_abroad_options');
        return view('attendees.edit', compact('attendee', 'events', 'event', 'pin_code', 'event_id','lived_abroad_options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->formRules);
        $attendee = Attendee::findOrfail($id);
        if (!$attendee)
        {
            abort('404');
        }
        $attendee = AttendeeService::setAttendeeRecords($request, $attendee);

        if ($attendee->save())
        {
            flash('Attendee was successfully updated!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('attendees.index', ['event' => $attendee->event]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attendee = Attendee::findOrfail($id);
        if ($attendee->delete())
        {
            flash('Attendee was successfully deleted!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('attendees.index');
    }

    /**
     * Show the import form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $event_id = $request->get('event');
        $pinCodeGen = new PinCode();
        $pin_code = $pinCodeGen->generate($event_id);
        $events = Event::all();
        return view('attendees.import', compact('events', 'event_id', 'pin_code'));
    }
    /**
     * Store a new imports resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeimport(Request $request)
    {
        $formRules = [
            'event' => 'required',
            'spreadsheet_file' => 'required',
        ];
        $this->validate($request, $formRules);
        if ($request->hasFile('spreadsheet_file'))
        {

            $path = $request->file('spreadsheet_file')->getRealPath();

            $data = Excel::load($path)->get();

            if ($data->count())
            {
                $attendeeCount = 0;
                foreach ($data as $key => $value)
                {
                    $attendeeRecord = [
                        'full_name' => $value->full_name,
                        'phone' => $value->phone,
                        'email' => $value->email,
                        'company' => $value->company,
                        'position' => $value->position,
                        'gender' => $value->gender,
                        'table_number' => $value->table_number,
                        'age' => $value->age,
                        'location' => $value->location,
                        'level_education' => $value->level_education,
                        'lived_abroad' => $value->lived_abroad,
                        'attending_as' => $value->attending_as,
                        'place_from' => $value->place_from,
                        'degree_type' => $value->degree_type,
                        'certificate_type' => $value->certificate_type,
                        'interest_area' => $value->interest_area,
                        'own_business_entrepreneur' => $value->own_business_entrepreneur,
                        'lived_abroad_period' => $value->lived_abroad_period,
                        'lived_abroad_country' => $value->lived_abroad_country,
                        'find_out_event' => $value->find_out_event,
                        'guest_represent' => $value->guest_represent,
                        'event' => $request->input('event'),
                        'mode' => $request->input('mode'),
                        'status' => $request->input('status'),
                        'registration' => $request->input('registration'),
                    ];

                    $attendee = Attendee::Create($attendeeRecord);
                    if ($attendee)
                    {
                        $pinCodeGen = new PinCode();

                        $pin_code = (isset($value->pin_code) && $value->pin_code != null) ? $value->pin_code : $pinCodeGen->generate($attendee->event);
                        AttendeeEvent::Create(['event_id' => $attendee->event, 'attendee_id' => $attendee->id, 'pin_code' => $pin_code]);
                        AttendeeService::generateQRCode($attendee->Event, $attendee, $pin_code);
                    }
                    $attendeeCount++;
                }

                if (!empty($attendeeRecord))
                {
                    flash('[' . $attendeeCount . '] ' . str_plural('Attendee', $attendeeCount) . ' ' . str_plural('was', $attendeeCount) . ' successfully added!')->success();
                }
                return redirect()->route('attendees.index', ['event' => $request->input('event')]);
            }

        }

        return redirect()->route('attendees.index');
    }

    function export(Request $request, $event_id)
    {
        AttendeeService::export($request, $event_id);

        return redirect()->route('attendees.index', ['event' => $event_id]);
    }

    function clearall(Request $request, $event_id)
    {
        if ($event_id)
        {
            $attendees = Attendee::whereEvent($event_id)->delete();
            flash('Attendee records deleted!')->success();
        }

        return redirect()->route('attendees.index', ['event' => $event_id]);
    }
}
