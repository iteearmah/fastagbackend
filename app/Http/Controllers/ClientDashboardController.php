<?php

namespace App\Http\Controllers;

use Auth;

class ClientDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->terms == 0)
        {
            return view('clientportal.agreement');
        }
        return redirect()->route('clientevents.index');
        //return view('clientportal.dashboard');
    }
}
