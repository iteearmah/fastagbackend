<?php

namespace App\Http\Controllers;

use App\Attendee;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		/*$attendee = Attendee::with('Event')->whereId('add067a4-7629-4f11-b1e6-967f72a5a3c9')->first();
		
		$otherData['event'] = $attendee->Event;
		
		$otherData['attendee'] = $attendee;
		$full_name = $otherData['attendee']['full_name'];
		$company = $otherData['attendee']['company'];
		 return view('partials.printag', compact('otherData','full_name','company'));
		*/
        return redirect()->route('dashboard');
       
    }
}
