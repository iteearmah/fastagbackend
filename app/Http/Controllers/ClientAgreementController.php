<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class ClientAgreementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->terms == 0)
        {
            return view('clientportal.agreement');
        }
        return view('clientportal.dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'terms' => 'required',
        ]);
        $user = Auth::user();
        $user->terms = ($request->input('terms') == 'on') ? 1 : 0;
        if ($user->save())
        {
            return redirect()->back();
        }
        else
        {
            flash('Error!')->error();
            return redirect()->route('client.agreement');
        }
    }
}
