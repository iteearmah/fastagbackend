<?php

namespace App\Http\Controllers;

use App\Attendee;
use App\Event;
use App\Events\AttendeeClockedIn;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$attendee = Attendee::find(1);
        //$event    = Event::find(1);
        //event(new AttendeeClockedIn($event, $attendee));
        return redirect()->route('events.index');
        //return view('dashboard');
    }
}
