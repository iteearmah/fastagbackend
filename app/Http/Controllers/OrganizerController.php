<?php

namespace App\Http\Controllers;

use App\Organizer;
use App\User;
use Illuminate\Http\Request;
use Image;
use Redirect;
use Storage;

class OrganizerController extends Controller
{

    protected $formRules;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->formRules = [
            'name' => 'required',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizers = Organizer::orderBy('created_at','DESC')->paginate(10);
        return view('organizers.index', compact('organizers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accounts = User::role('Organizer')->get();
        return view('organizers.create', compact('accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->formRules);
        $organizer                = new Organizer();
        $organizer->account       = $request->input('account');
        $organizer->name          = $request->input('name');
        $organizer->email         = $request->input('email');
        $organizer->phone         = $request->input('phone');
        $organizer->status        = $request->input('status');
        $organizer->address       = $request->input('address');
        $organizer->website       = $request->input('website');
        $organizer->logo_filename = $this->doLogoUpload($request);
        $organizer->save();

        if ($organizer->save())
        {
            flash('Organizer was successfully added!')->success();
        }
        else
        {
            flash('Error!')->error();
        }

        if ($request->filled('back'))
        {
            return Redirect::to($request->input('back'));
        }
        return redirect()->route('organizers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organizer = Organizer::findOrfail($id);
        if (!$organizer)
        {
            abort('404');
        }
        return view('organizers.show', compact('organizer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organizer = Organizer::findOrfail($id);
        $accounts  = User::role('Organizer')->get();
        if (!$organizer)
        {
            abort('404');
        }
        return view('organizers.edit', compact('organizer', 'accounts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->formRules);
        $organizer          = Organizer::find($id);
        $organizer->account = $request->input('account');
        $organizer->name    = $request->input('name');
        $organizer->email   = $request->input('email');
        $organizer->phone   = $request->input('phone');
        $organizer->status  = $request->input('status');
        $organizer->website = $request->input('website');
        $organizer->address = $request->input('address');
        if ($request->hasFile('logo_filename'))
        {
            $organizer->logo_filename = $this->doLogoUpload($request);
        }

        if ($organizer->save())
        {
            flash('Organizer was successfully updated!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('organizers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organizer = Organizer::findOrfail($id);
        if ($organizer->delete())
        {
            flash('Organizer was successfully deleted!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('organizers.index');
    }

    public function doLogoUpload($request)
    {
        if ($request->hasFile('logo_filename'))
        {
            //Storage::makeDirectory(public_path() . '/' . config('main.organizer_logo_dir'));
            $photoPath    = public_path() . '/' . config('main.organizer_logo_dir');
            $path         = $request->logo_filename->path();
            $extension    = $request->logo_filename->extension();
            $originalName = $request->logo_filename->getClientOriginalName();
            $filesize     = $request->logo_filename->getClientSize();
            $mime_type    = $request->logo_filename->getClientMimeType();

            $filename = uniqid() . '.' . $extension;

            //Generate width 100 photo file
            $img = Image::make($request->logo_filename->getRealPath());
            $img->save($photoPath . DIRECTORY_SEPARATOR . $filename);
            $media_width  = $img->width();
            $media_height = $img->height();
            //$img->fit(100);
            $img->resize(100, 100, function ($constraint)
            {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
            $img->save($photoPath . DIRECTORY_SEPARATOR . '100.' . $filename);

            //Generate width 300 photo file
            $img = Image::make($request->logo_filename->getRealPath());
            //$img->crop(340, 200);
            //$img->insert(public_path('images/logo.png'));
            $img->resize(320, null, function ($constraint)
            {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
            $img->save($photoPath . DIRECTORY_SEPARATOR . 'medium.' . $filename);

            return $filename;
        }
        return null;
    }
}
