<?php

namespace App\Http\Controllers;

use App\Event;
use App\EventGuest;
use Excel;
use Illuminate\Http\Request;

class EventGuestController extends Controller
{
    protected $formRules;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->formRules = [
            'event' => 'required',
            'full_name' => 'required|min:2|max:255',
            'phone' => 'required|regex:/[0-9]{15}/|digits:15',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $event = null;
        if ($request->filled('event'))
        {
            $event = Event::find($request->input('event'));
            $eventguests = EventGuest::filter($request)->get();
        }
        else
        {
            $eventguests = EventGuest::filter($request)->paginate(10)->appends(request()->query());
        }


        return view('eventguests.index', compact('eventguests', 'event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $event_id = $request->get('event');
        $events = Event::all();

        return view('eventguests.create', compact('events', 'event_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->formRules);

        return redirect()->route('attendees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eventguest = EventGuest::findOrfail($id);
        if ($eventguest->delete())
        {
            flash('Event Guest was successfully deleted!')->success();
        } else
        {
            flash('Error!')->error();
        }

        return redirect()->route('eventguests.index');
    }

    /**
     * Show the import form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $event_id = $request->get('event');
        $events = Event::all();

        return view('eventguests.import', compact('events', 'event_id'));
    }

    /**
     * Store a new imports resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeimport(Request $request)
    {
        $formRules = ['event' => 'required', 'spreadsheet_file' => 'required|min:2|max:255',];
        $this->validate($request, $formRules);
        if ($request->hasFile('spreadsheet_file'))
        {

            $path = $request->file('spreadsheet_file')->getRealPath();

            $data = Excel::load($path)->get();

            if ($data->count())
            {
                $eventGuestCount = 0;
                foreach ($data as $key => $value)
                {
                    $eventGuestRecord = [
                        'name' => $value->name,
                        'table_number' => $value->table_number,
                        'seats_on_table' => $value->seats_on_table,
                        'type' => $value->type,
                        'position' => $value->position,
                        'event_id' => $request->input('event'),
                        'mode' => $request->input('mode'),
                        'status' => $request->input('status'),
                    ];

                    $eventGuest = EventGuest::Create($eventGuestRecord);
                    if ($eventGuest)
                    {

                    }
                    $eventGuestCount++;
                }

                if ($eventGuest)
                {
                    flash('[' . $eventGuestCount . '] ' . str_plural('Event Guest',
                            $eventGuestCount) . ' ' . str_plural('was',
                            $eventGuestCount) . ' successfully added!')->success();
                }

                return redirect()->route('eventguest.index',['event' => $request->input('event')]);
            }
        }

        return redirect()->route('eventguest.index');
    }
}
