<?php

namespace App\Http\Controllers;

use App\Attendee;
use App\AttendeeEvent;
use App\Event;
use App\Events\AttendeeClockedIn;
use App\Organizer;
use App\Services\Event as EventService;
use App\PrintField;
use http\Exception\InvalidArgumentException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Image;
use Shivella\Bitly\Facade\Bitly;
use Storage;
use Symfony\Component\Translation\Exception\InvalidResourceException;

class EventController extends Controller
{
    protected $formRules;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->formRules = [
            'title' => 'required',
            'organizer' => 'required',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $organizer = null;
        if ($request->filled('organizer'))
        {
            $organizer = Organizer::find($request->input('organizer'))->orderBy('created_at','DESC');
        }
        $events = Event::with('Organizer')->withCount(['attendee_event'])->orderBy('created_at','DESC')->filter($request)->paginate(10);
        return view('events.index', compact('events', 'organizer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $printFields = PrintField::FIELDS;
        $organizers = Organizer::all();
        return view('events.create', compact('organizers','printFields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(EventService $eventService, Request $request)
    {
        $this->validate($request, $this->formRules);
        $event = new Event();
        $event->event_code = $eventService->generateCode();
        $event = $this->eventRecord($event, $request);

        $event->cover_photo = $this->doLogoUpload($request);
        if ($request->hasFile('programme_file'))
        {
            $event->programme_file = $this->programmeUpload($request);
        }
        if ($event->save())
        {
            EventService::savePrintFields($event->id, $request->input('print_fields'));
            try {
                \App\Services\Event::generateShortUrl($event->event_code);
            }
            catch (InvalidResourceException $exception)
            {

            }

            flash('Event was successfully added!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $event = Event::with('Organizer', 'attendee_event', 'event_guests')->findOrfail($id);
        if (!$event)
        {
            abort('404');
        }

        $attendees = Attendee::eventattendee($event->id)->where('mode', '=', 'live')->has('Attendance')->get();
        $event_days = EventService::event_days($event->id);
        $attendee_attendances = EventService::atteedee_attendance_days($event->id);
        $event_day_count = count($event_days);

        return view('events.show', compact('event', 'attendees', 'event_days', 'event_day_count', 'attendee_attendances'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $printFields = PrintField::FIELDS;
        $printFieldRecord = PrintField::whereEventId($id)->first() ?? new PrintField();
        $event = Event::with('Organizer')->findOrfail($id);
        $organizers = Organizer::all();
        if (!$event)
        {
            abort('404');
        }
        return view('events.edit', compact('event', 'organizers','printFields','printFieldRecord'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->formRules);
        $event = Event::findOrfail($id);
        if (!$event)
        {
            abort('404');
        }
        $organizers = Organizer::all();

        $event = $this->eventRecord($event, $request);
        EventService::savePrintFields($id, $request->input('print_fields'));
        if ($request->hasFile('programme_file'))
        {
            $event->programme_file = $this->programmeUpload($request);
        }
        if ($request->hasFile('cover_photo'))
        {
            $event->cover_photo = $this->doLogoUpload($request);
        }

        if ($event->save())
        {
            flash('Event was successfully update!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('events.edit', $event);
    }

    private function eventRecord($event, $request)
    {
        //$event_form_fields= json_decode($request->input('event_form_fields'));
        $event->title = $request->input('title');
        $event->theme = $request->input('theme');
        $event->organizer = $request->input('organizer');
        $event->venue = $request->input('venue');
        $event->excerpt = $request->input('excerpt');
        $event->start_date = $request->input('start_date');
        $event->end_date = $request->input('end_date');
        $event->details = $request->input('event_details');
        $event->background_color = $request->input('background_color');
        $event->font_color = $request->input('font_color');
        $event->programme_file_url = $request->input('programme_file_url');
        $event->app_form_type = ($request->input('app_form_type') === 'on') ? 'pin' : 'input';
        $event->app_field_full_name = ($request->input('app_field_full_name') === 'on') ? 'on' : 'off';
        $event->app_field_phone = ($request->input('app_field_phone') === 'on') ? 'on' : 'off';
        $event->app_field_email = ($request->input('app_field_email') === 'on') ? 'on' : 'off';
        $event->app_field_company = ($request->input('app_field_company') === 'on') ? 'on' : 'off';
        $event->app_field_position = ($request->input('app_field_position') === 'on') ? 'on' : 'off';
        $event->app_field_gender = ($request->input('app_field_gender') === 'on') ? 'on' : 'off';
        $event->app_field_table_number = ($request->input('app_field_table_number') === 'on') ? 'on' : 'off';
        $event->app_field_level_education = ($request->input('app_field_level_education') === 'on') ? 'on' : 'off';
        $event->app_field_age = ($request->input('app_field_age') === 'on') ? 'on' : 'off';
        $event->app_field_location = ($request->input('app_field_location') === 'on') ? 'on' : 'off';
        $event->app_field_country =  ($request->input('app_field_country') === 'on') ? 'on' : 'off';
        $event->app_field_level_education = ($request->input('app_field_level_education') === 'on') ? 'on' : 'off';
        $event->app_field_lived_abroad = ($request->input('app_field_lived_abroad') === 'on') ? 'on' : 'off';
        $event->app_field_attending_as = ($request->input('app_field_attending_as') === 'on') ? 'on' : 'off';
        $event->sms_message = $request->input('sms_message');
        $event->tag_image_option = $request->input('tag_image_option');

        return $event;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $event = Event::findOrfail($id);
        if ($event->delete())
        {
            flash('Event was successfully deleted!')->success();
        }
        else
        {
            flash('Error!')->error();
        }
        return redirect()->route('events.index');
    }

    public function doLogoUpload($request)
    {
        if ($request->hasFile('cover_photo'))
        {
            //Storage::makeDirectory(public_path() . '/' . config('main.cover_photo_dir'));
            $photoPath = public_path() . '/' . config('main.cover_photo_dir');
            $path = $request->cover_photo->path();
            $extension = $request->cover_photo->extension();
            $originalName = $request->cover_photo->getClientOriginalName();
            $filesize = $request->cover_photo->getClientSize();
            $mime_type = $request->cover_photo->getClientMimeType();

            $filename = uniqid() . '.' . $extension;

            //Generate width 100 photo file
            $img = Image::make($request->cover_photo->getRealPath());
            $img->save($photoPath . DIRECTORY_SEPARATOR . $filename);
            $media_width = $img->width();
            $media_height = $img->height();
            //$img->fit(100);
            $img->resize(100, 100, function ($constraint)
            {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
            $img->save($photoPath . DIRECTORY_SEPARATOR . '100.' . $filename);

            //Generate width 300 photo file
            $img = Image::make($request->cover_photo->getRealPath());
            //$img->crop(340, 200);
            //$img->insert(public_path('images/logo.png'));
            $img->resize(320, null, function ($constraint)
            {
                $constraint->aspectRatio();
                //$constraint->upsize();
            });
            $img->save($photoPath . DIRECTORY_SEPARATOR . 'medium.' . $filename);

            return $filename;
        }
        return null;
    }

    protected function programmeUpload($request)
    {
        if ($request->hasFile('programme_file'))
        {
            $file = $request->file('programme_file');
            $filename = 'programme-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('/' . config('main.programme_dir')), $filename);
            return $filename;
        }
        return null;
    }

    /**
     * Send sms to attendees.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function sms(Request $request, $id)
    {
        $event = Event::with('Organizer', 'attendee_event')->findOrfail($id);
        if (!$event)
        {
            abort('404');
        }
        return view('events.sms', compact('event'));
    }

    /**
     * Send SMS to destination.
     *
     * @param Request $request
     * @return Response
     */
    public function sendsms(Request $request, $id)
    {
        $event = Event::find($id);
        $event->sms_message = $request->input('sms_message');
        $event->save();
        $attendee_events = AttendeeEvent::with('event', 'attendee')->eventid($id)->whereHas('attendee', function ($query)
        {
            $query->where('attendees.deleted_at', '=', null);
        })->get();

        if (!$attendee_events)
        {
            abort('404');
        }
        foreach ($attendee_events as $attendee_event)
        {
            \Log::info("Request fired");
            event(new AttendeeClockedIn($attendee_event));
            \Log::info("Request ended");
        }
        flash('SMS sent to all attendees of ' . $event->title)->success();
        return redirect()->route('events.show', $id);
    }

    public function attendees_clockedin($id = null)
    {
        if ($id)
        {
            $event = Event::findOrfail($id);
            if (!$event)
            {
                flash('Event not found!')->error();
                return redirect()->route('events.show', ['id' => $id]);
            }
            EventService::setAttendanceCache($id, $event->event_code);
            flash('Event Attendance count refreshed!')->success();
            return redirect()->back();
        }
    }
}
