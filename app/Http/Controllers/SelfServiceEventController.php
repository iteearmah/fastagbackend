<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Resources\EventResource;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Cache;

class SelfServiceEventController extends Controller
{
    public function index($event_code, Request $request)
    {
        $event = Event::whereEventCode($event_code)->first();
        $event = new EventResource($event);
        if (!$event) {
            abort(404);
        }
        $loginDetails = $this->loginUser();
        $mode = ($request->has('mode')) ? $request->input('mode') : 'live';

        return view('selfservice.event-form', compact('event', 'loginDetails', 'mode'));
    }

    public function home(Request $request)
    {
        return view('selfservice.home');
    }

    private function loginUser()
    {
        $cacheKey = 'self_care_user';
        $success = Cache::get($cacheKey);

        if (!$success) {

            if (Auth::attempt(['email' => 'admin@enclaveafricasystems.com', 'password' => config('main.selfcare_password')])) {
                $user = Auth::user();
                $createToken = $user->createToken($user->name, ['save-attendee']);
                $token = $createToken->token;
                $token->expires_at = \Carbon\Carbon::now()->addHour(24);
                $token->save();
                $success['token'] = $createToken->accessToken;
                $success['user_uuid'] = $user->uuid;
                Cache::put($cacheKey,  $success,(60*24));
            }
        }

        return $success;
    }
}
