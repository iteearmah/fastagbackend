<?php

namespace App\Http\Controllers;

use App\Attendee;
use App\Event;
use App\Services\Event as EventService;
use App\Services\AttendeeSessionsService;
use Auth;

class ClientEventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->terms == 0)
        {
            return redirect()->route('client.agreement');
        }
        $events = Event::with('Organizer', 'attendees', 'attendee_event')->has('attendee_event')->account(Auth::user()->id)->whereHas('attendees', function ($q)
        {
            $q->where('mode', '=', 'live');
        })->get();
        return view('clientportal.events', compact('events'));
    }

    public function attendees($uuid = null)
    {
        if (Auth::user()->terms == 0)
        {
            return redirect()->route('client.agreement');
        }
        $event = Event::uuid($uuid);
        // $attendees            = Attendee::with('Event', 'attendee_event')->where('event', '=', $event->id)->get();
        $attendees = Attendee::with('Attendance', 'attendee_event')->has('attendee_event')->eventattendee($event->id)->where('mode', '=', 'live')->orderBy('full_name', 'asc')->get();
        $event_days = EventService::event_days($event->id);
        $attendee_attendances = EventService::atteedee_attendance_days($event->id);
        $event_day_count = count($event_days);
        $attendeeSessions = AttendeeSessionsService::eventAttendeeSessions($event->id);
        return view('clientportal.attendees', compact('attendees', 'event', 'event_days', 'event_day_count', 'attendee_attendances', 'attendeeSessions'));
    }
}
