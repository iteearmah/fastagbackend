<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttendeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'full_name' => $this->full_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'company' => $this->company,
            'position' => $this->position,
            'gender' => $this->gender,
            'table_number' => $this->table_number,
            'age' => $this->age,
            'location' => $this->location,
            'level_education' => $this->level_education,
            'lived_abroad' => $this->lived_abroad,
            'attending_as' => $this->attending_as,
        ];
    }

}
