<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventGuestResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'event_id' => $this->event_id,
            'table_number' => $this->phone,
            'seats_on_table' => $this->seats_on_table,
            'seats_occupied' => $this->event_guest_attendees()->count() ,
            'type' => $this->type,
        ];
    }
}
