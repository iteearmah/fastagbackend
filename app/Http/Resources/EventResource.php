<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'event_code' => $this->event_code,
            'title' => $this->title,
            'theme' => $this->theme,
            'cover_photo' => (isset($this->cover_photo)) ? asset(config('main.cover_photo_dir') . '/' . $this->cover_photo) : null,
            'background_color' => $this->background_color,
            'font_color' => $this->font_color,
            'form_type' => $this->app_form_type,
            'field_full_name' => $this->app_field_full_name,
            'field_phone' => $this->app_field_phone,
            'field_email' => $this->app_field_email,
            'field_company' => $this->app_field_company,
            'field_position' => $this->app_field_position,
            'field_gender' => $this->app_field_gender,
            'field_table_number' => $this->app_field_table_number,
            'field_age' => $this->app_field_age,
            'field_location' => $this->app_field_location,
            'field_country' => $this->app_field_country,
            'field_level_education' => $this->app_field_level_education,
            'field_lived_abroad' => $this->app_field_lived_abroad,
            'field_attending_as' => $this->app_field_attending_as,
            'guest_company' => $this->event_guests()->type('company')->select('id', 'name')->get(),
            'guest_individual' => $this->event_guests()->type('individual')->select('id', 'name')->get(),
        ];

    }

    public function with($request)
    {
        return [
            'status' => 'success',
            'message' => 'Event record found',
        ];
    }
}
