<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncPrintFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:print_folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $interval = 5;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->start();
    }

    private function runIt(): void
    { // Your function to run every 5 seconds
        $localPrintDirectory = config('main.print_dir');
        $syncCommand = 'rsync -e "ssh -o StrictHostKeyChecking=no" -chazP --password-file=<(echo "'.config('main.server_password').'") --stats m324dyiz@108.179.242.164:/home4/m324dyiz/fastag.enclaveafrica.com/public/print/* '. $localPrintDirectory;
        exec($syncCommand);
    }

    private function start(): void
    {
        $active = true;
        $nextTime   = microtime(true) + $this->interval; // Set initial delay

        while($active) {
            usleep(1000); // optional, if you want to be considerate

            if (microtime(true) >= $nextTime) {
                $this->runIt();
                $nextTime = microtime(true) + $this->interval;
            }
            // Do other stuff (you can have as many other timers as you want)

            $active = !$this->checkForStopFlag();
        }
    }
    // /opt/php71/bin/php

    private function checkForStopFlag(): bool
    { // completely optional
        // Logic to check for a program-exit flag
        // Could be via socket or file etc.
        // Return TRUE to stop.
        return false;
    }

}
