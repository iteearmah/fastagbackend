<?php

namespace App\Console\Commands;

use App\Jobs\DataSyncJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DataSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:sync {--action=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync data to and from production platform';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if ($this->option('action'))
        {
            $action = $this->option('action');
            DataSyncJob::dispatch($action);
        } else
        {
            $this->info('Command should be data:sync --action=pull or data:sync --action=push');
        }
    }
}
