<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class Attendance extends Model
{
    use SoftDeletes;
    protected $table    = 'attendances';
    protected $fillable = ['event_id', 'attendee_id', 'clocked_in_date', 'clocked_in_time', 'clocked_out_date', 'clocked_out_time', 'mode'];
    protected $dates    = ['clocked_in_date', 'clocked_out_date'];
    public $incrementing = false;
    protected $primaryKey = 'id';

    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->id = Uuid::uuid4()->toString();
        });
    }

    public function event()
    {
        return $this->belongsTo('App\Event', 'event_id', 'id');
    }

    public function attendee()
    {
        return $this->belongsTo('App\Attendee', 'attendee_id', 'id');
    }

    public function scopeEventID($query, $event_id = null)
    {
        $query->where('event_id', $event_id ?: $this->event_id);
    }

    public function scopeEventDays($query, $event_id = null)
    {
        $query->distinct('clocked_in_date')->eventid($event_id);
    }

}
