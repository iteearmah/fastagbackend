<?php

namespace App\Listeners;

use App;
use App\Events\PrintTag;
use Illuminate\Contracts\Queue\ShouldQueue;

class PrintAttendeeTag implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PrintTag  $event
     * @return void
     */
    public function handle(PrintTag $event)
    {
        //\Log::debug($event->pin_code);
        //Storage::makeDirectory(public_path() . '/' . config('main.print_dir'));
        $printPath = config('main.print_dir');
        $pdf = App::make('dompdf.wrapper');
        $printHtml = view('partials.printag', ['full_name' => $event->full_name, 'company' => $event->company, 'organizer_logo' => $event->organizer_logo, 'otherData' => $event->otherData]);
        //\Log::debug("TEST OTHER DATA: " . print_r($event->otherData,true));
        $pdf->loadHTML($printHtml);
        $pdf->setPaper([0, 0, 198.425, 141.732], 'portrait');
        $tag_filename = (isset($event->tag_filename) && !empty($event->tag_filename)) ? $event->tag_filename : $event->pin_code;
        return $pdf->save($printPath . DIRECTORY_SEPARATOR . $tag_filename . '.pdf');
    }
}
