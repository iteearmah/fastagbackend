<?php

namespace App\Listeners;

use App\Events\AttendeeClockedIn;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendAttendeeSMS implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AttendeeClockedIn  $event
     * @return void
     */
    public function handle(AttendeeClockedIn $event)
    {
        //\Log::debug($event->attendee_event->event->title);
        //\Log::debug($event->attendee_event->attendee->phone);
        $attendee_phone = $event->attendee_event->attendee->phone;
        if (str_contains($event->attendee_event->attendee->phone, '233'))
        {
            $attendee_phone = $event->attendee_event->attendee->phone;
        }
        else
        {
            $attendee_phone = '233' . ltrim($event->attendee_event->attendee->phone, 0);
        }

        $postData = [
            'from' => config('app.name'),
            'to'   => "$attendee_phone",
            'text' => $this->notifyMessage($event->attendee_event),
        ];
        //dd($postData);
        $response = $this->sendSMSMessage($postData);
        //\Log::debug($attendee_phone . ' | ' . $event->attendee_event->event->title . ' | ' . $response->getStatusCode());

    }

    protected function sendSMSMessage($postData)
    {
        $client   = new Client();

        $defaultSMSProvider = config('sms.default_provider');
        switch ($defaultSMSProvider){
            case 'jteksms':
                $smsURL = config('sms.jteksms.url') . '&' . http_build_query([
                    'username' => config('sms.jteksms.username'),
                    'password' => config('sms.jteksms.password'),
                    'destination' => $postData['to'],
                    'source' => $postData['from'],
                    'message' => $postData['text'],
                ]);
                $request = $client->get($smsURL, [
                    'connect_timeout' => 3.14,
                ]);

                $response = $request->getBody()->getContents();
                Log::info('responsesmsURL: ' . $response);
                Log::info('smsURL: ' . $smsURL);
                break;
            default:
                $response = $client->post(config('sms.infobip.url'), [
                    'headers' => ['Accept' => 'application/json', 'Content-type' => 'application/json'],
                    'body'    => json_encode($postData),
                    'auth'    => [config('sms.infobip.username'), config('sms.infobip.password')],
                    'connect_timeout' => 3.14,
                ]);
                //Log::info('smsInfoBIPURL: ' . config('sms.infobip.url'));
                break;
        }

        return $response;
    }
    protected function notifyMessage($attendee_event)
    {
        $msg         = '';
        $event_title = $attendee_event->event->title;
        if ($attendee_event->event->programme_file_url)
        {
            $event_link = $attendee_event->event->programme_file_url;
        }
        else
        {
            $event_link = route('promopageredirect', $attendee_event->event->event_code);
        }
        $event_message = str_replace(['[EVENT_TITLE]', '[EVENT_LINK]', '[ATTENDEE_PIN_CODE]'], [$event_title, $event_link, $attendee_event->pin_code], $attendee_event->event->sms_message);

        $msg .= $event_message;
        return $msg;
    }
}
