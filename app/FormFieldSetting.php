<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use Yajra\Auditable\AuditableTrait;

class FormFieldSetting extends Model
{
    use SoftDeletes, AuditableTrait;
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $fillable = ['event_form_id','label','required_field'];
    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->id = Uuid::uuid4()->toString();
        });
    }
}
