<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrintField extends Model
{
    protected $guarded = [];
    public const FIELDS = [
        'full_name' => [
            'key' => 'full_name',
            'name' => 'Full Name'
        ],
        'company' => [
            'key' => 'company',
            'name' => 'Company'
        ],
        'phone' => [
            'key' => 'phone',
            'name' => 'Phone'
        ],
        'position' => [
            'key' => 'position',
            'name' => 'Position'
        ],
    ];

    public function event_guest()
    {
        return $this->belongsTo('App\EventGuest', 'event_guest_id', 'id');
    }

    public function attendee()
    {
        return $this->belongsTo('App\Attendee', 'attendee_id', 'id');
    }
}
