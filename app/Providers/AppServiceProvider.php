<?php

namespace App\Providers;

use App\Contracts\Services\IDataSyncJobService;
use App\Services\DataSyncJobService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
        $this->app->bind(IDataSyncJobService::class, DataSyncJobService::class);
    }
}
