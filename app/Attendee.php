<?php

namespace App;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use Yajra\Auditable\AuditableTrait;

class Attendee extends Model
{
    use SoftDeletes, AuditableTrait, Uuids;
    protected $table = 'attendees';
    protected $guarded = [];
    //protected $fillable = ['full_name', 'uuid', 'event', 'phone', 'email', 'company', 'position', 'mode', 'gender', 'registration', 'status', 'pin_code', 'table_number', 'age', 'location', 'level_education', 'lived_abroad', 'attending_as'];
    public $incrementing = false;
    protected $primaryKey = 'id';

    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->id = Uuid::uuid4()->toString();
        });
    }

    public function Event()
    {
        return $this->belongsTo('App\Event', 'event', 'id');
    }

    public function guest()
    {
        return $this->belongsTo('App\EventGuest', 'attendee_id', 'id');
    }

    public function attendee_event()
    {
        return $this->belongsTo('App\AttendeeEvent', 'id', 'attendee_id');
    }

    public function Attendance()
    {
        return $this->hasMany('App\Attendance', 'attendee_id', 'id');
    }

    public function scopeFilter($query, $request)
    {
        if ($request->filled('event'))
        {
            $event = $request->input('event');
            $query->where('event', '=', $event);
        }

        if ($request->has('search') && $request->filled('search'))
        {
            $search = $request->input('search');
            $terms = explode(' ', $search);
            $query->where(function ($query) use ($terms)
            {
                if ($terms)
                {
                    foreach ($terms as $term)
                    {
                        $query->orWhere('full_name', 'like', '%' . $term . '%');
                    }
                }

            });
        }
        return $query;
    }

    public function scopeEventAttendee($query, $event_id)
    {
        return $query->whereHas('attendee_event', function ($query) use ($event_id)
        {
            $query->where('attendee_events.event_id', '=', $event_id);
        });

    }

}
