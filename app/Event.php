<?php

namespace App;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use Yajra\Auditable\AuditableTrait;

class Event extends Model
{
    use SoftDeletes, AuditableTrait, Uuids;
    protected $table    = 'events';
    protected $fillable = ['title', 'organizer', 'theme', 'venue', 'excerpt', 'cover_photo', 'details', 'start_date', 'start_time', 'end_date', 'end_time', 'price_tag', 'status', 'event_code','tag_image_option'];
    protected $dates    = ['start_date', 'end_date'];
    protected $softDelete = true;
    public $incrementing = false;
    protected $primaryKey = 'id';

    public static function boot()
    {
         parent::boot();
         self::creating(function($model){
             $model->id = Uuid::uuid4()->toString();
         });
    }

    public function attendee_event()
    {
        return $this->hasMany('App\AttendeeEvent', 'event_id', 'id');
    }

    public function event_guests()
    {
        return $this->hasMany('App\EventGuest', 'event_id', 'id');
    }

    public function attendees()
    {
        return $this->hasManyThrough('App\Attendee', 'App\AttendeeEvent', 'event_id','id','id','attendee_id');
    }


    public function Organizer()
    {
        return $this->belongsTo('App\Organizer', 'organizer', 'id');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance', 'event_id', 'id');
    }

    public function printField()
    {
        return $this->hasOne('App\PrintField', 'event_id', 'id');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = ($value != null) ? \Carbon\Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d') : null;
    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = ($value != null) ? \Carbon\Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d') : null;
    }

    public function scopeFilter($query, $request)
    {
        if ($request->filled('organizer'))
        {
            $organizer = $request->input('organizer');
            $query->where('organizer', '=', $organizer);
        }
        return $query;
    }

    public function scopeStatus($query, $value = 'active')
    {
        $query->where('status', '=', $value);
        return $query;
    }
    public function scopeHasAttendance($query)
    {
        return $query->whereHas('attendances', function ($query2)
        {
            $query2->whereSum('event_id', '>', 0);
        });
    }

    public function scopeAccount($query, $account_id = 0)
    {
        return $query->whereHas('Organizer', function ($query) use ($account_id)
        {
            $query->where('account', $account_id);
        });
    }

    /**
     * Get a json decoded version of the form_builder_json string
     *
     * @return array
     */
   /* public function getFormBuilderArrayAttribute($value) : array
    {
        return json_decode($this->attributes['form_builder_json'], true);
    }*/
}
