<?php

include './api/config/database.php';

if (!empty($_GET['event'])) {
	$event_id = "AND event_id = '".$_GET['event']."'";
}else {
	$event_id = "";
}

$sql = "SELECT COUNT(id) counter FROM feedbacks WHERE rating = '5'".$event_id;
$results = mysqli_query($con,$sql);

if ($results->num_rows > 0) {
    $five_star = $results ->fetch_assoc();
    $five_star = $five_star['counter'];
}

$sql = "SELECT COUNT(id) counter FROM feedbacks WHERE rating = '4'".$event_id;
$results = mysqli_query($con,$sql);
if ($results->num_rows > 0) {
    $four_star = $results ->fetch_assoc();
    $four_star = $four_star['counter'];
}

$sql = "SELECT COUNT(id) counter FROM feedbacks WHERE rating = '3'".$event_id;
$results = mysqli_query($con,$sql);
if ($results->num_rows > 0) {
    $three_star = $results ->fetch_assoc();
    $three_star = $three_star['counter'];
}

$sql = "SELECT COUNT(id) counter FROM feedbacks WHERE rating = '2'".$event_id;
$results = mysqli_query($con,$sql);
if ($results->num_rows > 0) {
    $two_star = $results ->fetch_assoc();
    $two_star = $two_star['counter'];
}

$sql = "SELECT COUNT(id) counter FROM feedbacks WHERE rating = '1'".$event_id;
$results = mysqli_query($con,$sql);
if ($results->num_rows > 0) {
    $one_star = $results ->fetch_assoc();
    $one_star = $one_star['counter'];
}
mysqli_close($con);
?>
<html>
<head></head>
<body>

<div id="pieChart"></div>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/4.7.2/d3.min.js"></script>-->
<script src="d3.min.js"></script>
<script src="d3pie.min.js"></script>
<script>
var pie = new d3pie("pieChart", {
	"header": {
		"title": {
			"text": "Event Ratings by attendees.",
			"fontSize": 24,
			"font": "open sans"
		},
		"subtitle": {
			"text": "A full pie chart to show off label collision detection and resolution.",
			"color": "#999999",
			"fontSize": 12,
			"font": "open sans"
		},
		"titleSubtitlePadding": 9
	},
	"footer": {
		"color": "#999999",
		"fontSize": 10,
		"font": "open sans",
		"location": "bottom-left"
	},
	"size": {
        "canvasWidth": 700,
        "canvasHeight":600,
		"pieOuterRadius": "90%"
	},
	"data": {
		"sortOrder": "value-desc",
		"content": [
			{
				"label": "5 Star",
				"value": <?=$five_star?>,
				"color": "#6eab26"
			},
			{
				"label": "4 Star",
				"value": <?=$four_star?>,
				"color": "#89ff00"
			},
			{
				"label": "3 Star",
				"value": <?=$three_star?>,
				"color": "#ff0000"
			},
			{
				"label": "2 Star",
				"value": <?=$two_star?>,
				"color": "#ff6700"
			},
			{
				"label": "1 Star",
				"value": <?=$one_star?>,
				"color": "#ff6700"
			}
		]
	},
	"labels": {
		"outer": {
            "format": "label-value2",
			"pieDistance": 20
		},
		"inner": {
			"hideWhenLessThanPercentage": 3
		},
		"mainLabel": {
			"fontSize": 17
		},
		"percentage": {
			"color": "#ffffff",
            "decimalPlaces": 0,
            "fontSize": 17
		},
		"value": {
			"color": "#adadad",
			"fontSize": 18
		},
		"lines": {
			"enabled": true
		},
		"truncation": {
			"enabled": true
		}
	},
	"effects": {
		"pullOutSegmentOnClick": {
			"effect": "linear",
			"speed": 400,
			"size": 8
		}
	},
	"misc": {
		"gradient": {
			"enabled": true,
			"percentage": 100
		}
	}
});
</script>

</body>
</html>