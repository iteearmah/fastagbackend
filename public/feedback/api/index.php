<?php
header('Content-Type: application/json');

include 'config/database.php';
// print_r($con);exit;
$input = json_decode(file_get_contents('php://input'),TRUE);
if (empty($_POST)) {
    $data = $input;
}else{
    $data = $_POST;
}

// echo json_encode($data);exit;

if (empty($data['f'])) {
    echo json_encode(array('ERR'=>'Function missing'));exit;
}

if ($data['f'] == 'reactionSave') {
    $sql = "INSERT INTO feedbacks (event_id,mac_address,rating) VALUES ('".mysqli_escape_string($con,$data['event_id'])."','".mysqli_escape_string($con,$data['mac_address'])."','".mysqli_escape_string($con,$data['reaction_type'])."')";
    $results = mysqli_query($con,$sql);

    if ($results > 0) {
        $response = array('id'=>mysqli_insert_id($con),'res'=>'Feedback Recorded Successfully');
    }else{
        $response = array('ERR'=>'Error occured');
    }

    echo json_encode($response);
}

if ($data['f'] == 'event_verify') {
    $sql = "SELECT * FROM events WHERE event_code =  ".mysqli_escape_string($con,$data['event_code']);

    $results = mysqli_query($con,$sql);

    if ($results->num_rows > 0) {
        $response = $results ->fetch_assoc();
    }else {
        $response = array('ERR'=>'Event not found');
    }

    echo json_encode($response);
}

mysqli_close($con);
?>