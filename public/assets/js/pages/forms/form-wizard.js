$(function () {
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
    //Advanced form with validation
    var form = $('#wizard_with_validation').show();
    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
            $.eagle.input.activate();

            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid(); 

        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
                       /*var upload_application_form = $('#upload_application_form').prop('files')[0]['name'];
                       var upload_result_slip = $('#upload_result_slip').prop('files')[1]['name'];
                       var upload_recommendation_letter = $('#upload_recommendation_letter').prop('files')[2]['name'];*/
                        var values = {};
                         var form_data= new FormData();
                        $.each($('#wizard_with_validation input'), function(i, field) {
                            values[field.name] = field.value;
                            //form_data.append(field.name,field.value);
                        });     

                        form_data.append('values',JSON.stringify(values));
                        form_data.append('upload_application_form',$('#upload_application_form').prop('files')[0]);
                        form_data.append('upload_result_slip',$('#upload_result_slip').prop('files')[0]);
                        form_data.append('upload_recommendation_letter',$('#upload_recommendation_letter').prop('files')[0]);
                        //swal({'title':'Saving form...',showCancelButton: false,showConfirmButton:false});
                        $.ajax({
                            type: 'POST',
                            url: "/applicationform/ajaxstore",
                            contentType:false,
                            cache:false,
                            processData:false,
                            data: form_data,
                            dataType: 'json',
                            success: function (data) {
                               /* console.log(data);*/
                                if (data.response === 'success') 
                                {
                                     swal({
                                            title: "Application Form",
                                            text: "Submitted!",
                                            type: "success"
                                        }, function() {
                                            window.location = '/applicationform';
                                        });
                                }
                                else if (data.response === 'error') 
                                {
                                    swal(data.message, "Not Submitted!", "error");
                                }
                                /*if (data.response === 'ok') 
                                {                 
                                    move = true;

                                    $("#example-async").steps("setStep", 2);

                                } else {

                                    move = false;
                                }
*/
                            }
                        });              
                        

            
        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            /*'christian_names': 'required',
            'surname': 'required',
            'birth_date': 'required',
            'country': 'required',
            'sex': 'required',
            'district_origin': 'required',
            'institution_name': 'required',
            'entry_date': 'required',
            'course': 'required',
            'level': 'required',
            'stage_reached': 'required',
            'sponsoring_organisation': { required: '#study_leave[value="yes"]:checked'},
            'award_type': 'required',
            'specific_subject': 'required',
            'scholarship_type': { required: '#scholarship_held[value="yes"]:checked'},
            'duration': { required: '#scholarship_held[value="yes"]:checked'},
            'course_scholoarship_awarded': { required: '#scholarship_held[value="yes"]:checked'},
            'bankers_name': 'required',
            'position_in_class': { required: '#applicant_recommended_scholarship[value="yes"]:checked'},
            'subject_to_enable_qualification': { required: '#applicant_recommended_scholarship[value="yes"]:checked'},
            'name_of_principal':'required',
            'registrar_signed_date':'required',
            'official_vetting_name':'required',
            'recommendation_by_registrar':'required',
            'approved_date':'required'*/
     
    
            
        }//rules
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}

