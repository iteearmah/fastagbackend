const request = require('request');
let server = require('ws').Server;
require('dotenv').config();
let Memcached = require('memcached');
let memcached = new Memcached('127.0.0.1:11211');
let redis = require('redis');
let redisClient = redis.createClient(); // this creates a new client
const cacheKeyPrefix = process.env.CACHE_PREFIX;
const defaultCacheServer = 'redis';
let s = new server({
    port: 9000
});
let number_event_attendees;
let cacheKey = cacheKeyPrefix + ':number_event_attendees_';
redisClient.on('connect', function() {
    console.log('Redis client connected');
});
redisClient.on('error', function(err) {
    console.log('Something went wrong ' + err);
});
/*redisClient.get(cacheKey + "nl8G34", function(error, result) {
    if (error) {
        console.log(error);
        throw error;
    }
    console.log(cacheKey + "nl8G34: " + result);
});*/

function getRecord(cacheServer = 'memcached', cacheKey, client) {
    if (cacheServer == 'redis') {
        redisClient.get(cacheKey, function(error, result) {
            if (error) {
                console.log(error);
                throw error;
            }
            client.send(JSON.stringify({
                identifier: client.clientIdentifier,
                data: result
            }));
            console.log("Redis Sent: " + JSON.stringify({
                identifier: client.clientIdentifier,
                data: result
            }));
        });
    } else if (cacheServer == 'memcached') {
        memcached.get(cacheKey + client.clientIdentifier, function(err, data) {
            client.send(JSON.stringify({
                identifier: client.clientIdentifier,
                data: data
            }));
            console.log("Sent: " + JSON.stringify({
                identifier: client.clientIdentifier,
                data: data
            }));
        });
    }
}
s.on('connection', function(ws) {
    let myInterval = setInterval(function() {
        s.clients.forEach(function e(client) {
            if ((client != ws) || 1) {
                if (typeof client.clientIdentifier !== 'undefined') {
                    getRecord(defaultCacheServer, cacheKey + client.clientIdentifier, client);
                }
            }
        });
    }, 800);
    ws.on('message', function(message) {
        message = JSON.parse(message);
        if (message.type == 'identifier') {
            ws.clientIdentifier = message.data;
            return;
        }
        console.log("Received: " + JSON.stringify(message));
        s.clients.forEach(function e(client) {
            if ((client != ws) || 1) {
                console.log(message.toIdentifier + " " + client.clientIdentifier);
                if (typeof message.toIdentifier !== 'undefined' && message.toIdentifier == client.clientIdentifier) {
                    getRecord(defaultCacheServer, cacheKey + client.clientIdentifier, client);
                }
            }
        });
    });
    ws.on('close', function() {
        console.log("Client Disconnected!");
    })
    ws.on("pong", function() { // we received a pong from the client.
        ws.pingssent = 0; // reset ping counter.
    });
});