<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    protected $toTruncate = ['model_has_roles', 'users', 'model_has_permissions', 'permissions', 'roles', 'role_has_permissions'];
    public function run()
    {
        foreach ($this->toTruncate as $table)
        {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            DB::table($table)->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        }
        $this->call(RolesAndPermissionsSeeder::class);
    }
}
