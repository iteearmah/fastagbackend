<?php

use App\User;
use Illuminate\Database\Seeder;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\PersonalAccessClient;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'create_organizer']);
        Permission::create(['name' => 'edit_organizer']);
        Permission::create(['name' => 'delete_organizer']);
        Permission::create(['name' => 'update_organizer']);

        // create permissions
        Permission::create(['name' => 'create_attendee']);
        Permission::create(['name' => 'edit_attendee']);
        Permission::create(['name' => 'delete_attendee']);
        Permission::create(['name' => 'update_attendee']);

        // create roles and assign existing permissions
        $role = Role::create(['name' => 'Admin']);
        $role->givePermissionTo('create_organizer');
        $role->givePermissionTo('edit_organizer');
        $role->givePermissionTo('delete_organizer');
        $role->givePermissionTo('update_organizer');

        $role = Role::create(['name' => 'Attendee']);
        $role->givePermissionTo('create_attendee');
        $role->givePermissionTo('edit_attendee');
        $role->givePermissionTo('delete_attendee');
        $role->givePermissionTo('update_attendee');

        $role = Role::create(['name' => 'Organizer']);
        $role->givePermissionTo('edit_organizer');

        $user           = new User();
        $user->name     = 'System Admin';
        $user->email    = 'admin@admin.com';
        $user->phone    = '0248291821';
        $user->password = Hash::make('td43GsVy');
        $user->save();
        $mainUserID = $user->id;
        $user->assignRole('Admin');
        $clientrepository        = new ClientRepository();
        $client                  = $clientrepository->createPersonalAccessClient($user->id, $user->name, config('app.url'));
        $accessClient            = new PersonalAccessClient();
        $accessClient->client_id = $client->id;
        $accessClient->save();

        /*$user             = new User();
        $user->name       = 'Organizer One';
        $user->email      = 'iteearmah@gmail.com';
        $user->phone      = '0248291829';
        $user->password   = Hash::make('123456');
        $user->created_by = $mainUserID;
        $user->updated_by = $mainUserID;
        $user->save();
        $user->assignRole('Organizer');
        $clientrepository        = new ClientRepository();
        $client                  = $clientrepository->createPersonalAccessClient($user->id, $user->name, config('app.url'));
        $accessClient            = new PersonalAccessClient();
        $accessClient->client_id = $client->id;
        $accessClient->save();*/

    }
}
