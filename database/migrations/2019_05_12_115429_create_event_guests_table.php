<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_guests', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index()->unique();
            $table->unsignedInteger('event_id')->index()->nullable();
            $table->enum('type', ['company','individual'])->default('company');
            $table->string('name')->nullable();
            $table->integer('table_number')->default(0);
            $table->integer('seats_on_table')->default(0);
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('mode', ['live', 'test'])->default('live');
            $table->auditable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_guests');
    }
}
