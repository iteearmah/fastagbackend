<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendees', function (Blueprint $table)
        {
            $table->increments('id');
            $table->uuid('uuid')->index()->unique();
            $table->unsignedInteger('event')->index()->nullable();
            $table->string('full_name')->nullable();
            $table->string('phone')->index()->nullable();
            $table->string('email')->index()->nullable();
            $table->string('company')->nullable();
            $table->string('position')->nullable();
            $table->string('gender')->nullable();
            $table->enum('mode', ['live', 'test'])->default('live');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('registration', ['pin', 'input'])->default('input');
            $table->auditable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendees');
    }
}
