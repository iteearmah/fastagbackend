<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_id')->index();
            $table->tinyInteger('full_name')->default(1);
            $table->tinyInteger('company')->default(1);
            $table->tinyInteger('phone')->default(0);
            $table->tinyInteger('position')->default(0);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_fields');
    }
}
