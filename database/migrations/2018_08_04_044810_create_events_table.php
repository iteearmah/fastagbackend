<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table)
        {
            $table->increments('id');
            $table->uuid('uuid')->index()->unique();
            $table->char('event_code', 100)->index()->nullable();
            $table->string('title');
            $table->unsignedInteger('organizer')->index()->nullable();
            $table->mediumText('theme')->nullable();
            $table->string('venue')->nullable();
            $table->string('excerpt')->nullable();
            $table->string('cover_photo')->nullable();
            $table->text('details')->nullable();
            $table->string('programme_file')->nullable();
            $table->string('programme_file_url')->nullable();
            $table->date('start_date')->nullable();
            $table->time('start_time')->nullable();
            $table->date('end_date')->nullable();
            $table->time('end_time')->nullable();
            $table->string('price_tag')->nullable();
            $table->char('background_color', 10)->index()->nullable();
            $table->char('font_color', 10)->index()->nullable();
            $table->enum('app_form_type', ['input', 'pin'])->default('input');
            $table->enum('app_field_full_name', ['on', 'off'])->default('on');
            $table->enum('app_field_phone', ['on', 'off'])->default('on');
            $table->enum('app_field_email', ['on', 'off'])->default('on');
            $table->enum('app_field_company', ['on', 'off'])->default('on');
            $table->enum('app_field_position', ['on', 'off'])->default('on');
            $table->enum('app_field_gender', ['on', 'off'])->default('off');
            $table->string('sms_message', 1600)->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->auditable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
