<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherJobFairFieldsToAttendeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendees', function (Blueprint $table) {
            $table->string('place_from')->nullable()->after('lived_abroad')->comment('Where are you from');
            $table->string('degree_type')->nullable()->after('place_from')->comment('If you hold a Bachelor or Master degree, Kindly specify the degree');
            $table->string('certificate_type')->nullable()->after('degree_type')->comment('If you had a Vocational or Technical Training, Please specify the type of certificate');
            $table->string('interest_area')->nullable()->after('certificate_type')->comment('Your areas of Interest');
            $table->string('own_business_entrepreneur')->nullable()->after('interest_area')->comment('Would you like to start your own business and become an entrepreneur');
            $table->string('lived_abroad_country')->nullable()->after('own_business_entrepreneur')->comment('If yes – what country did you live in');
            $table->string('lived_abroad_period')->nullable()->after('lived_abroad_country')->comment('If yes – how long did you live abroad');
            $table->string('find_out_event')->nullable()->after('lived_abroad_period')->comment('How did you find out aboutHow did you find out about event');
            $table->string('guest_represent')->nullable()->after('find_out_event')->comment('Do you represent a company, institution or media house');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendees', function (Blueprint $table) {
            $table->dropColumn('place_from');
            $table->dropColumn('degree_type');
            $table->dropColumn('certificate_type');
            $table->dropColumn('interest_area');
            $table->dropColumn('own_business_entrepreneur');
            $table->dropColumn('lived_abroad_country');
            $table->dropColumn('lived_abroad_period');
            $table->dropColumn('find_out_event');
            $table->dropColumn('guest_represent');
        });
    }
}
