<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('event_id')->index();
            $table->unsignedInteger('attendee_id')->index();
            $table->date('clocked_in_date')->index()->nullable();
            $table->time('clocked_in_time')->index()->nullable();
            $table->date('clocked_out_date')->index()->nullable();
            $table->time('clocked_out_time')->index()->nullable();
            $table->enum('mode', ['live', 'test'])->default('live');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
