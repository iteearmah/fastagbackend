@extends('layouts.public')
@section('title'){{$event->title.' - Welcome - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid" style="padding: 0px; margin: 0px;">
    <style type="text/css">
        .logo-box {
		margin: 20px auto 20px auto;
    	max-width: 640px;
		text-align: center;
	}
    .logo-box img{ max-height: 150px;}
	.middle-section{
		min-height: 40%;
	    padding: 24px 0;
	    background: {{$event->background_color ?? '#A32F54'}};
	    text-align: center;
	    align-items: center;
	    justify-content: center;

	}
	.middle-section h1{ font-size: 25px }
	.middle-section h2{font-weight: 100; font-size: 23px}
	.middle-section h1,h2,h3,h4{color:{{$event->font_color ?? '#fff'}} !important;}
	.welcome-text{font-size: 3.5em;}
    .theme-text{font-size: 18px;line-height: 24px;font-weight: 100;}
	.footer-section{text-align: center; padding: 20px 0;}
    </style>
    <div class="logo-box">
        <img src="{{ asset(config('main.cover_photo_dir').'/medium.'.$event->cover_photo )}}" >
        </img>
    </div>
    <div class="middle-section">
        <h1>
            {{$event->title}}
        </h1>
        @if($event->theme)
        <h4 class="theme-text">Theme: {{$event->theme}}</h4>
        @endif
        <h2>
           {{($event->start_date)? $event->start_date->format('dS F ,Y') : null}}
            {{($event->end_date && $event->end_date->greaterThan($event->start_date))? ' - ' . $event->end_date->format('dS F ,Y') : null}}
            <br>
                {{$event->venue}}
            </br>
        </h2>
        <h3 class="welcome-text">
            You are welcome
        </h3>
        <h2>
            The event brochure has information you should see
        </h2>
        <div>
            @php
            if(isset($event->programme_file_url))
            {
                $downloadLink= $event->programme_file_url;
            }
            else
            {
                $downloadLink= asset(config('main.programme_dir').'/'.$event->programme_file);
            }
            @endphp
            <a class="btn btn-primary btn-lg" href="{{ $downloadLink }}" download style="background-color: #0c0c0c; color: #fff;">
                <i class="fa fa-file-pdf-o"></i> Download the Programme
            </a>
        </div>
    </div>
    <div class="footer-section">
    	Powered by Enclave Africa
    </div>
</div>
@endsection
