@extends('layouts.app')

@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Edit User
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('users.index')}}">
                    User Administration
                </a>
            </li>
            <li class="active">
                Edit {{$user->name}}
            </li>
        </ol>
    </div>

     <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                                @include('errors.list')
                                {{ Form::model( $user, ['route' => ['users.update', $user->id], 'method' => 'put', 'role' => 'form'] ) }}
                         <div class="form-group">
                          {{ Form::label('name', 'Name') }}
                          {{ Form::text('name', null, array('class' => 'form-control')) }}
                      </div>
                      <div class="form-group">
                          {{ Form::label('phone', 'Phone') }}
                          {{ Form::text('phone', null, array('class' => 'form-control')) }}
                      </div>
                    <div class="form-group">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', null, array('class' => 'form-control')) }}
                    </div>
                    {{-- <div class="form-group">
                        {{ Form::label('password', 'Confirm Password') }}
                        <br>
                            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
                        </br>
                    </div> --}}

                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            Give Role
                        </label>
                    </div>
                    <div class="form-group">
                        
                    <ul class="list-inline">
                        @php
                        $user_roles=$user->roles->pluck('id')->toArray();
                        @endphp
                        @foreach ($roles as $role)
                         <li class="eagle-checkbox">
                                        <label class="eagle-check custom-checkbox">
                             <input type="checkbox" name="roles[]" class="eagle-check-input" value="{{ $role['id']}}" {{(in_array($role['id'],$user_roles))? 'checked':''}} />
                            <span class="eagle-check-indicator"></span>
                            <span class="eagle-check-description">{{ucfirst($role->name)}}</span>
                            </label>
                            </li>
                        @endforeach
                    </ul>   
                    </div>
                </div>
                 {{ Form::submit('Save', array('class' => 'btn btn-primary  pull-right')) }}
                  {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
