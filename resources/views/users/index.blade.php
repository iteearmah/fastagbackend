@extends('layouts.app')
@section('title'){{'Users - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            User Administration 
            <a href="{{route('users.create')}}" class="btn btn-primary"><i class="fa fa-plus" ></i> Add New</a>  
            <a class="btn btn-default" href="{{ route('roles.index') }}">Roles</a>
            <a class="btn btn-default" href="{{ route('permissions.index') }}">Permissions</a>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li class="active">
                User Administration
            </li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                
                <div class="panel-body">
                     @if(Session::has('flash_message'))
                <div class="alert alert-success"> {!! session('flash_message') !!} </div>
                @endif 
                    @include('errors.list')
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="pull-right">{{ $users->links() }}</div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Date/Time Added
                                    </th>
                                    <th>
                                        Roles
                                    </th>
                                    <th>
                                        Operations
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>
                                        {{ $user->name }}
                                    </td>
                                    <td>
                                        {{ $user->phone }}
                                    </td>
                                    <td>
                                        {{ $user->email }}
                                    </td>
                                    <td>
                                        {{ $user->created_at->format('F d, Y h:ia') }}
                                    </td>
                                    <td>
                                        {{  $user->roles()->pluck('name')->implode(' ') }}
                                    </td>
                                    {{-- Retrieve array of roles associated to a user and convert to string --}}
                                    <td>
                                        <div class="col-md-* pull-left">
                                            <a class="btn btn-warning pull-left" href="{{ route('users.edit', $user->id) }}" style="margin-right: 3px;">
                                                <i class="fa fa-pencil">
                                                </i>
                                                Edit
                                            </a>
                                        </div>
                                        <div class="col-md-* pull-left">
                                            <form action="{{ route('users.destroy',['id'=>$user->id]) }}" method="post">
                                                {{csrf_field()}}
                                                <input name="_method" type="hidden" value="DELETE"/>
                                                <input name="id" type="hidden" value="{{ $user->id }}">
                                                    <button class="btn btn-danger" type="submit">
                                                        <i class="fa fa-trash">
                                                        </i>
                                                    </button>
                                                </input>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                         <div class="pull-right">{{ $users->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
