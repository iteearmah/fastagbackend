@extends('layouts.app')

@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Add User
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('users.index')}}">
                    User Administration
                </a>
            </li>
            <li class="active">
                Add User
            </li>
        </ol>
    </div>

     <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                                @include('errors.list')
                   {{ Form::open(array('route' => 'users.store')) }}
{{ Form::hidden('back', Request::get('back')) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('phone', 'Phone', array('class' => 'control-label')) }}
        {{ Form::text('phone', '', array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', '', array('class' => 'form-control')) }}
    </div>

    <div class='form-group'>
        <ul class="list-inline">
        
        @if(Request::get('mode'))
           @foreach ($roles as $role)
           @if(Request::get('mode')==$role->name)
         <li class="eagle-checkbox">
                        <label class="eagle-check custom-checkbox">
             <input type="checkbox" name="roles[]" checked="checked" class="eagle-check-input" value="{{ $role['id']}}" />
            <span class="eagle-check-indicator"></span>
            <span class="eagle-check-description">{{ucfirst($role->name)}}</span>
            </label>
            </li>
            @endif
        @endforeach
        @else
        @foreach ($roles as $role)
         <li class="eagle-checkbox">
                        <label class="eagle-check custom-checkbox">
             <input type="checkbox" name="roles[]" class="eagle-check-input" value="{{ $role['id']}}" />
            <span class="eagle-check-indicator"></span>
            <span class="eagle-check-description">{{ucfirst($role->name)}}</span>
            </label>
            </li>
        @endforeach
        @endif
        
    </ul>
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}<br>
        {{ Form::password('password', array('class' => 'form-control')) }}

    </div>

    <div class="form-group">
        {{ Form::label('password', 'Confirm Password') }}<br>
        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

    </div>

    {{ Form::submit('Add', array('class' => 'btn btn-primary  pull-right')) }}

    {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
