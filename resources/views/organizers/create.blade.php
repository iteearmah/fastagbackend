@extends('layouts.app')
@section('title'){{'Organizers - Add New - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Organizers - Add New
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('organizers.index')}}">
                    Organizers
                </a>
            </li>
            <li class="active">
                Add New
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <form action="{{route('organizers.store')}}" class="" enctype="multipart/form-data" method="POST">
                            <input type="hidden" name="back" value="{{ Request::get('back') }}">
                            @include('organizers.form',['organizer_id'=>null])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
