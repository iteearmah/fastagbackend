@extends('layouts.app')
@section('title'){{$organizer->name.' - Organizers - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Organizers - View [#{{$organizer->id}}]
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('organizers.index')}}">
                    Organizers
                </a>
            </li>
            <li class="active">
                {{$organizer->name}}
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <dl class="dl-vertical showview">
                            <dt>Name</dt>
                            <dd>{{$organizer->name}}</dd>
                            <dt>Email</dt>
                            <dd>{{$organizer['email'] ?? 'N/A'}}</dd>
                            <dt>Phone</dt>
                            <dd>{{$organizer['phone'] ?? 'N/A'}}</dd>
                            <dt>Website</dt>
                            <dd>{{$organizer['website'] ?? 'N/A'}}</dd>
                            <dt>Address</dt>
                            <dd>{{$organizer['address'] ?? 'N/A'}}</dd>
                            <dt>Status</dt>
                            <dd>{{ucwords($organizer['status']) ?? 'N/A'}}</dd>
                            <dt>Created</dt>
                            <dd>{{$organizer->created_at->format('j, M Y')}}</dd>
                            <dt>Logo</dt>
                            <dd>
                                @if(isset($organizer->logo_filename))
                                <div class="fileinput-preview thumbnail col-md-3">
                                    <img alt="Logo" height="auto" id="photo_preview" onerror="this.src='{{asset('photos/nophoto.jpg')}}'" src="{{ asset(config('main.organizer_logo_dir').'/medium.'.$organizer->logo_filename )}}"  />
                                </div>
                                @endif
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
