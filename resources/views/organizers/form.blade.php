{{csrf_field()}}
 @include('partials.organizeraccount-select')
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class=" control-label" for="name">
        Name
    </label>
    <input autofocus="" class="form-control" id="name" name="name" placeholder="Organizer Name" type="text" value="{{ old('name',$organizer->name ?? null ) }}"/>
    @if ($errors->has('name'))
    <span class="help-block">
        <strong>
            {{ $errors->first('name') }}
        </strong>
    </span>
    @endif
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    <label class=" control-label" for="name">
        Phone Number
    </label>
    <input class="form-control" id="phone" name="phone" placeholder="Organizer Phone Number" type="text" value="{{ old('phone',$organizer->phone ?? null ) }}"/>
    @if ($errors->has('phone'))
    <span class="help-block">
        <strong>
            {{ $errors->first('phone') }}
        </strong>
    </span>
    @endif
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label class=" control-label" for="name">
        Email
    </label>
    <input class="form-control" id="email" name="email" placeholder="Organizer Email" type="email" value="{{ old('email',$organizer->email ?? null ) }}"/>
    @if ($errors->has('email'))
    <span class="help-block">
        <strong>
            {{ $errors->first('email') }}
        </strong>
    </span>
    @endif
</div>
<div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
    <label class=" control-label" for="website">
        Website
    </label>
    <input class="form-control" id="website" name="website" placeholder="Organizer Website" type="text" value="{{ old('website',$organizer->website ?? null ) }}"/>
    @if ($errors->has('website'))
    <span class="help-block">
        <strong>
            {{ $errors->first('website') }}
        </strong>
    </span>
    @endif
</div>
<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label class=" control-label" for="address">
        Address
    </label>
    <textarea  class="form-control" id="address" name="address" placeholder="Organizer Address">{{ old('address',$organizer->address ?? null ) }}</textarea>
    @if ($errors->has('address'))
    <span class="help-block">
        <strong>
            {{ $errors->first('address') }}
        </strong>
    </span>
    @endif
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class=" control-label" for="name">
        Status
    </label>
    @php
        $organizer_active_status=((isset($organizer->status) && $organizer->status=='active') || !isset($organizer->status) )? ' checked':'';
        $organizer_inactive_status=((isset($organizer->status)) && $organizer->status=='inactive')? ' checked':'';
    @endphp
    <input class="with-gap" id="status_active" name="status" type="radio" value="active" {{$organizer_active_status}}/>
    <label for="status_active">
        Active
    </label>
    <input class="with-gap" id="status_inactive" name="status" type="radio" value="inactive" {{$organizer_inactive_status}} />
    <label class="m-l-20" for="status_inactive">
        Inactive
    </label>
    <span class="help-block">
        <strong>
            {{ $errors->first('status') }}
        </strong>
    </span>
</div>
<div class="form-group{{ $errors->has('logo_filename') ? ' has-error' : '' }} ">
    <div class="fileinput fileinput-new" data-provides="fileinput">
        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
             @if(isset($organizer->logo_filename))
            <img alt="Logo" height="auto" id="photo_preview" onerror="this.src='{{asset('photos/nophoto.jpg')}}'" src="{{ asset(config('main.organizer_logo_dir').'/medium.'.$organizer->logo_filename )}}"  />
            @endif

        </div>
        <div>
            <span class="btn btn-info btn-file"><span class="fileinput-new">Select Logo</span>
            <span class="fileinput-exists">Change</span><input accept="image/gif, image/jpg, image/jpeg, image/png" class="form-control" name="logo_filename"  type="file"></span>
            <a href="#" class="btn btn-default bg-red fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
        @if ($errors->has('logo_filename'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('logo_filename') }}
                </strong>
            </span>
        @endif
    </div>
</div>



<div class="form-group">
    <input class="btn btn-primary pull-right" name="submit" type="submit" value="Submit"/>
</div>


