@extends('layouts.app')
@section('title'){{'Organizers - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Organizers <a href="{{route('organizers.create')}}" class="btn btn-primary"><i class="fa fa-plus" ></i> Add New</a>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li class="active">
                Organizers
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    @include('flash::message')
                    <div class="row">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Phone Number
                                    </th>
                                    <th>
                                        Created
                                    </th>
                                    <th colspan="4">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($organizers as $organizer)
      
                                <tr>
                                    <td>
                                        {{$organizer['id']}}
                                    </td>
                                    <td>
                                        {{$organizer['name']}}
                                    </td>
                                    <td>
                                        {{$organizer['email'] ?? 'N/A'}}
                                    </td>
                                    <td>
                                        {{$organizer['phone'] ?? 'N/A'}}
                                    </td>
                                    <td>
                                        {{$organizer->created_at->format('d M ,Y h:ia')}}
                                    </td>
                                     <td>
                                        <a class="btn btn-success" href="{{ route('events.index', ['organizer'=>$organizer->id]) }}" title="Events">
                                            <i class="fa fa-calendar"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-default" href="{{ route('organizers.show', $organizer->id) }}" title="View">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('organizers.edit',$organizer->id) }}" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>

                                    <td>
                                        <form action="{{ route('organizers.destroy',['id'=>$organizer->id]) }}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-danger" type="submit" title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </input>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12">
                            Showing {{($organizers->currentpage()-1)* $organizers->perpage()+1}} to {{(($organizers->currentpage()-1) * $organizers->perpage())+$organizers->count()}} of {{$organizers->total()}} entries
                        </div>
                        <div class="pull-right">{{ $organizers->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
