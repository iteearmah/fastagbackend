@extends('layouts.app')
@section('title'){{$organizer->name.' - Organizers - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Organizers - Edit [#{{$organizer->id}}]
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li class="active">
                {{$organizer->name}}
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <form action="{{route('organizers.update',['id'=>$organizer->id])}}" class="" enctype="multipart/form-data" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            @include('organizers.form',['organizer_id'=>$organizer->id,'account_id'=>$organizer->account])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
