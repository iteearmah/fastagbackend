@extends('layouts.app')
@section('title'){{'Edit '.$permission->name.' Permission - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Edit Role: {{$permission->name}}
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('permissions.index')}}">
                    Permissions
                </a>
            </li>
            <li class="active">
                Edit Permission: {{$permission->name}}
            </li>
        </ol>
    </div>

     <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                             @include('errors.list')
                             {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}
                                        <div class="form-group">
                                            {{ Form::label('name', 'Permission Name') }}
                                {{ Form::text('name', null, array('class' => 'form-control')) }}
                                        </div>
                                        <br>
                                            {{ Form::submit('Edit', array('class' => 'btn btn-primary  pull-right')) }}

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
