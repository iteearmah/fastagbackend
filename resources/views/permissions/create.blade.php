@extends('layouts.app')
@section('title'){{'Add Permission - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Add Permission
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('permissions.index')}}">
                    Permissions
                </a>
            </li>
            <li class="active">
                Add Permission
            </li>
        </ol>
    </div>

     <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                                @include('errors.list')
              {{ Form::open(array('route' => 'permissions.store')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
    </div><br>
    @if(!$roles->isEmpty())
        <h4>Assign Permission to Roles</h4>

    <ul >
        @foreach ($roles as $role) 
        <li class="eagle-checkbox">
                        <label class="eagle-check custom-checkbox">
             <input type="checkbox" name="roles[]" class="eagle-check-input" value="{{ $role['id']}}" />
        <span class="eagle-check-indicator"></span>
            <span class="eagle-check-description">{{ucfirst($role->name)}}</span>
            </label>
            </li>
        @endforeach
    </ul>
    @endif
    <br>
    {{ Form::submit('Add', array('class' => 'btn btn-primary  pull-right')) }}

    {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
