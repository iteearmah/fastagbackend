@extends('layouts.app')
@section('title'){{'Permissions - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Permissions 
            <a href="{{route('permissions.create')}}" class="btn btn-primary"><i class="fa fa-plus" ></i> Add New</a>  
            <a class="btn btn-default" href="{{ route('users.index') }}">Users</a>
            <a class="btn btn-default" href="{{ route('roles.index') }}">Roles</a>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li class="active">
                Permissions
            </li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if(Session::has('flash_message'))
                    <div class="alert alert-success"> {!! session('flash_message') !!} </div>
                    @endif 
                    @include('errors.list')
                  <div class="table-responsive">
        <table class="table table-striped">

            <thead>
                <tr>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $permission->name }}</td> 
                    <td>
                    <a href="{{ route('permissions.edit',$permission->id)}}" class="btn btn-warning pull-left" style="margin-right: 3px;"><i class="fa fa-pencil"></i> Edit</a>

                    {!! Form::open(['method' => 'DELETE', 'route' => ['permissions.destroy', $permission->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
