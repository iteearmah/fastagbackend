<div class="form-group{{ $errors->has('lived_abroad') ? ' has-error' : '' }} ">
    <label class="col-form-label" for="select_lived_abroad">
       Lived in Abroad
    </label>
    <select class="form-control" id="select_lived_abroad" name="lived_abroad">
        <option value=""> ---Select---</option>
        @isset($lived_abroad_options)
        @foreach($lived_abroad_options as $lived_abroad_option)
        @php
        $livedAbroadSelected='';
        if(isset($attendee->lived_abroad) && $attendee->lived_abroad == $lived_abroad_option )
        {
            $livedAbroadSelected='selected="selected"';
        }
        elseif(old('lived_abroad') == $lived_abroad_option){
             $livedAbroadSelected='selected="selected"';
        }
        @endphp
        <option value="{{$lived_abroad_option}}" {{$livedAbroadSelected}}>{{$lived_abroad_option}}</option>
        @endforeach
            @endisset
    </select>
    @if ($errors->has('lived_abroad'))
    <span class="help-block">
        <strong>
            {{ $errors->first('lived_abroad') }}
        </strong>
    </span>
    @endif
</div>

@push('scripts')
<script type="text/javascript">
   $('#select_lived_abroad').select2({
        placeholder: '--Select--',
        allowClear: true,
        theme:'bootstrap',
    });
</script>
@endpush
