<div class="form-group{{ $errors->has('event') ? ' has-error' : '' }} ">
    <label class="col-form-label" for="select_event">
       Select Event
    </label>
    <select class="form-control" id="select_event" name="event">
        <option value="">
            Select Event
        </option>
        
        @foreach($events as $event)
        @php
        $eventSelected='';
        if((isset($event_id) && $event_id==$event->id))
        {
            $eventSelected='selected="selected"';
        }
        elseif(old('select_event')==$event){
             $eventSelected='selected="selected"';
        }
        @endphp
        <option value="{{$event->id}}" {{$eventSelected}}>{{$event->title}}</option>
        @endforeach
    </select>
    @if ($errors->has('event'))
    <span class="help-block">
        <strong>
            {{ $errors->first('event') }}
        </strong>
    </span>
    @endif
</div>

@push('scripts')
<script type="text/javascript">
   $('#select_event').select2({
        placeholder: 'Select Event',
        allowClear: true,
        theme:'bootstrap',
    });
</script>
@endpush