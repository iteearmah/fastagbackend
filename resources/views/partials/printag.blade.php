
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<head>
	<title>{{ $full_name ?? ' ' }} - {{ $company ?? ' ' }}</title>
	<style type="text/css">
		@page { size: 198.425pt 141.732pt;margin: 0px; }

		body {
			margin: 0px;
			padding: 0px;
			    font-family: 'Helvetica';
			}
        @if(isset($otherData['event']['tag_image_option']) && $otherData['event']['tag_image_option'] == 'qr_code')
            .name{font-size: 20px;margin-bottom: 2px !important; margin-top: 35px; display: block;}
        @else
            .name{font-size: 30px;margin-bottom: 2px !important; margin-top: 15px; display: block; }
        @endif
			.company{font-size: 16px; margin: 0px !important;font-weight: normal;display: block;}
            .location {font-size: 13px; margin-top: 0px !important;font-weight: normal;display: block;}
			.organizer_logo{ margin-top: 5px; height: auto; }
            .table_number{font-size: 12px; margin-top: 0px !important;font-weight: normal; font-weight: bold;}
            .no_logo{width:70px; height: 56px; display: block;}
            .qr_code{width:30px; height: 30px; display: block;}
            .no_logo,.qr_code{ margin: 8px auto 5px auto;}
            .img-only{margin-top:33px;}
			.printbox{ width:100%;height: 50%; }
			@if(isset($otherData['event']['tag_image_option']) && $otherData['event']['tag_image_option']!= 'qr_code')
				.printbox{margin-top: 20%;}
			@endif
			.printbox td{vertical-align: middle; text-align:center;}
			
	</style>
</head>
<table class="printbox"  align="center">
<tr>
<td>
    @if(!empty($otherData['event']) && !empty($otherData['event']->printField))
        @isset($otherData['event'])
        @if(isset($otherData['event']['tag_image_option']))
            @if($otherData['event']['tag_image_option'] == 'no_logo')
                <div class="no_logo_"></div>
            @elseif($otherData['event']['tag_image_option'] == 'qr_code')
                <div class="qr_code"><img src="{{ asset(config('main.qr_code_dir') . '/' . $otherData['pin_code'] .'.png') }}" width="70" height="auto" class="organizer_logo" /></div>
            @else
                @if($organizer_logo)
                    <img src="{{ $organizer_logo }}" width="70" height="auto"  class="organizer_logo" />
                @endif
            @endif
        @endif

            @if($otherData['event']->printField['full_name'])
                 <h1 class="name">{{ $full_name ?? ' ' }}</h1>
            @endif
            @if($otherData['event']->printField['phone'])
                <h3 class="company">{{ $otherData['attendee']->phone ?? '' }}</h3>
            @endif
            @if($otherData['event']->printField['position'])
                <h3 class="company">{{ $otherData['attendee']->position ?? '' }}</h3>
            @endif
            @if($otherData['event']->printField['company'])
                <h3 class="company">{{ $company ?? ' ' }}</h3>
             @endif
        @endisset
            @if(isset($otherData['location']) &&isset($otherData['location_to_country']) && $otherData['location_to_country'] == 'on') || $otherData['location'])
                <h4 class="location">{{ $otherData['location'] ?? ' ' }}</h4>
            @endif
        @if(isset($otherData['table_number']) && $otherData['table_number'] != 'null'  && $otherData['table_number'] != null)
        <h5 class="table_number">Table No. ({{$otherData['table_number']}})</h5>
        @endif
    @endif
	</td>
	</tr>
</table>
</html>

