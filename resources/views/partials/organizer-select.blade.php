<div class="form-group{{ $errors->has('organizer') ? ' has-error' : '' }} ">
    <label class="col-form-label" for="select_organizer">
       Select Organizer
    </label>
    <select class="form-control" id="select_organizer" name="organizer" onchange="this.selectedIndex==1 && (window.location = this.options[this.selectedIndex].value);">
        <option value="">
            Select Organizer
        </option>
        <option value="{{ route('organizers.create',['mode'=>'Event','back'=>URL::current()]) }}">[ Create New Organizer ]</option>
        @foreach($organizers as $organizer)
        @php
        $organizerSelected='';
        if((isset($organizer_id) && $organizer_id==$organizer->id))
        {
            $organizerSelected='selected="selected"';
        }
        elseif(old('select_organizer')==$organizer){
             $organizerSelected='selected="selected"';
        }
        @endphp
        <option value="{{$organizer->id}}" {{$organizerSelected}}>{{$organizer->name}}</option>
        @endforeach
    </select>
    @if ($errors->has('organizer'))
    <span class="help-block">
        <strong>
            {{ $errors->first('organizer') }}
        </strong>
    </span>
    @endif
</div>

@push('scripts')
<script type="text/javascript">
   $('#select_organizer').select2({
        placeholder: 'Select Organizer',
        allowClear: true,
        theme:'bootstrap',
    });
</script>
@endpush