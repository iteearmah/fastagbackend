<div class="form-group{{ $errors->has('account') ? ' has-error' : '' }} ">
    <label class="col-form-label" for="select_account">
        Login Account 
    </label>
    <select class="form-control" id="select_account" name="account" onchange="this.selectedIndex==1 && (window.location = this.options[this.selectedIndex].value);">
        <option value="">
            Login Account
        </option>
        <option value="{{ route('users.create',['mode'=>'Organizer','back'=>URL::current()]) }}">[ Create New Account ]</option>
        @foreach($accounts as $account)
        @php
        $accountSelected='';
        if((isset($account_id) && $account_id==$account->id))
        {
            $accountSelected='selected="selected"';
        }
        elseif(old('select_account')==$account){
             $accountSelected='selected="selected"';
        }
        @endphp
        <option value="{{$account->id}}" {{$accountSelected}}>{{$account->name}}</option>
        @endforeach
    </select>
    @if ($errors->has('account'))
    <span class="help-block">
        <strong>
            {{ $errors->first('account') }}
        </strong>
    </span>
    @endif
</div>

@push('scripts')
<script type="text/javascript">
   $('#select_account').select2({
        placeholder: 'Select Account',
        allowClear: true,
        theme:'bootstrap',
    });
</script>
@endpush