@extends('layouts.client')
@section('title'){{Auth::user()->name.' Agreement - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="card col-md-6" id="one">
        <div class="card-header">
            <h2>
                Client Agreement
            </h2>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-md-12">
                	@include('flash::message')
                    <form action="{{route('client.agreementstore')}}" method="POST">
                        {{csrf_field()}}
                        <div style="height: 400px; overflow: auto;" id="terms_box">
                        <strong><span style="font-size: x-large;">TERMS OF USE</span></strong></p>
             <p> I recognize and agree that this data (full names, phone numbers, emails, company names and positions) has been collated in accordance with the Ghana Data Protection Act , 2012 (Act 843) as a data controller.</p>

 <p>Enclave Africa System is there not responsible for the usage and improper dissemination of this data.</p>
 <p>The Processor shall comply with all provisions for the protection of Personal Data set out in this Data Processing Agreement and in applicable data protection legislation with relevance for Processing of Personal Data.</p>

 <p>The Processor shall comply with the instructions and routines issued by the Controller in relation to the Processing of Personal Data.</p>
The Processor shall only Process Personal Data on, and in accordance with, the instructions from the Controller.</p>

 <p>The Processor shall not Process Personal Data without a prior written agreement with the Controller or without written instructions from the Controller beyond what is necessary to fulfil its obligations towards the Controller under the Agreement.</p>
 <p><a href="https://www.dataprotection.org.gh/sites/default/files/Data%20Protection%20Act%20%2C%202012%20%28Act%20843%29.pdf">CLICK HERE TO DOWNLOAD THE DATAPROTECTION ACT</a> to understand your responsibility as a data processor.</p>
                        </div>
                        <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}" style="margin-top: 20px">
                            <ul class="list-inline">
                                <li class="eagle-checkbox">
                                    <label class="eagle-check custom-checkbox">
                                        <input class="eagle-check-input" name="terms" type="checkbox"/>
                                        <span class="eagle-check-indicator">
                                        </span>
                                        <span class="eagle-check-description">
                                            Agree with the terms and conditions
                                        </span>
                                    </label>
                                </li>
                            </ul>
                            @if ($errors->has('terms'))
                            <span class="help-block">
                                <strong>
                                    {{ $errors->first('terms') }}
                                </strong>
                            </span>
                        </div>
                        @endif
                        <div class="form-group">
                            <input class="btn btn-primary pull-right" name="submit" type="submit" value="I Agree" id="agreement_btn" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
     // Initially disable the button
          $("#agreement_btn").attr("disabled", "disabled");

          // Map the function below to the scroll event of our Terms DIV
          $("#terms_box").scroll(function() {
               if ($("#terms_box").AtEnd()) {
                    // Enable the button once we reach the end of the DIV
                    $("#agreement_btn").removeAttr("disabled");
               }
           });
     });

     $.fn.AtEnd = function() {
         return this[0].scrollTop + this.height() >= this[0].scrollHeight;
     }

</script>
@endpush
