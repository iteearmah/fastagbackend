@extends('layouts.client')
@section('title'){{ $event->title.' Attendees - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="col-md-12">
        <h3>
            {{ $event->title }}
        </h3>
    </div>

        <div class="col-md-12">
            <ul class="nav nav-tabs nav-tabs-noborder nav-justified" role="tablist">
                <li role="presentation" class="active"><a href="#event-attendees" data-toggle="tab">ATTENDANCE</a></li>
                <li role="presentation"><a href="#event-ratings" data-toggle="tab">EVENT RATINGS</a></li>
                <li role="presentation"><a href="#event-sessions" data-toggle="tab">SESSIONS</a></li>
            </ul>
            <div class="card">
                <div class="body">
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="event-attendees" role="tabpanel">
                            <div class="row">
                                <table class="table table-bordered table-striped table-hover attendee-table dataTable">
                                    @php
                                        $formFields=['full_name'=>'Full Name','phone'=>'Phone','email'=>'Email','company'=>'Company Name','position'=>'Position','gender'=>'Gender','table_number'=>'Table Number','age'=>'Age','location'=>'Location','level_education'=>'Highest Level of Education','lived_abroad'=>'Lived Abroad (6 months above)','attending_as'=>'Attending As'];
                                    @endphp
                                    <thead>
                                    <tr>
                                        {{-- <th>ID</th> --}}
                                        @foreach($formFields as $formKey =>$formLabel)
                                            @if($event['app_field_'.$formKey]=='on')
                                                <th>{{ $formLabel }}</th>
                                            @endif
                                        @endforeach
                                        {{-- <th>Pin Code</th> --}}
                                        @if($event_day_count > 0)
                                            @for ($i = 1; $i <= $event_day_count; $i++)
                                                <th>Date</th>
                                            @endfor
                                        @endif
                                        {{--  <th>Created At</th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($attendees)
                                        @foreach($attendees as $attendee)
                                            <tr>


                                                @foreach($formFields as $formKey =>$formLabel)
                                                    @if($event['app_field_'.$formKey]=='on')
                                                        @if($formKey === 'country')
                                                            @php
                                                                $countries = config('countries');
                                                            @endphp
                                                            <td>{{ (!empty($attendee[$formKey])) ? $countries[$attendee[$formKey]] : 'N/A'  }}</td>
                                                        @else
                                                            <td>{{ (!empty($attendee[$formKey]) &&  ($attendee[$formKey]!= 'null')) ? $attendee[$formKey] : 'N/A' }}</td>
                                                        @endif
                                                    @endif
                                                @endforeach
                                                {{--  <td>
                                                     {{$attendee->attendee_event->pin_code}}
                                                 </td> --}}
                                                @if($event_day_count)

                                                    @foreach($event_days as $event_day => $value)
                                                        <td>
                                                            @if(isset($attendee_attendances[$attendee->id]) && isset($attendee_attendances[$attendee->id][$event_day]))
                                                                {{-- <i class="fa  fa-check-circle text-success"><span class="visible-print">✓</span></i> --}}
                                                                <span class="small" style="width: 63px; display: block;">{!!$attendee_attendances[$attendee->id][$event_day]!!}</span>
                                                            @else
                                                                <i class="fa fa-times-circle text-danger"><span class="visible-print">×</span></i>
                                                            @endif
                                                        </td>
                                                    @endforeach
                                                @endif
                                                {{--  <td>
                                                     {{$attendee->created_at->format('d M ,Y h:ia')}}
                                                 </td> --}}
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="event-ratings" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <iframe src="https://feedback.enclaveafrica.com/?event={{ $event->uuid }}" width="750" height="700" frameborder="0"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="event-sessions" role="tabpanel">
                            <table class="table table-bordered table-striped table-hover attendee-sessions-table dataTable">
                                <thead>
                                <tr>
                                  <th>Attendee Name</th>
                                  <th>Phone Number</th>
                                    <th>Session Name</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @isset($attendeeSessions)
                                        @foreach($attendeeSessions as $attendeeSession)
                                            <tr>
                                                <td>{{$attendeeSession->attendee}}</td>
                                                <td>{{$attendeeSession->phone}}</td>
                                                <td>{{$attendeeSession->session_name}}</td>
                                                <td>
                                                    <div style="font-size: 12px; height: 50%;">{{\Carbon\Carbon::parse($attendeeSession->created_at)->format('d M ,Y')}}</div>
                                                    <div style="font-size: 13px; height: 50%;">{{\Carbon\Carbon::parse($attendeeSession->created_at)->format('h:ia')}}</div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@push('head')
<link href="{{ asset('plugins/summernote/dist/summernote.css') }}" rel="stylesheet"/>
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@push('scripts')
<script src="{{ asset('plugins/summernote/dist/summernote.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{ asset('plugins//jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.keyTable.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/responsive.bootstrap.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.scroller.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.fixedHeader.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('.attendee-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    $('.attendee-sessions-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
</script>
@endpush
