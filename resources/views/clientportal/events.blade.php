@extends('layouts.client')
@section('title'){{Auth::user()->name.' Events - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="card" id="one">
        <div class="card-header">
            <h2>
                 Events
            </h2>
        </div>
        <div class="body">
        	<div class="row">
        		<table class="table table-bordered table-striped table-hover event-table dataTable">
                            <thead>
                                <tr>
                                    <th>
                                        Code
                                    </th>
                                    <th>
                                        Title
                                    </th>
                                    <th>
                                        No. of Attendees
                                    </th>
                                    <th>
                                        Start Date
                                    </th>
                                    <th>
                                        End Date
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($events as $event)

                                <tr>
                                    <td>
                                        {{$event['event_code']}}
                                    </td>
                                    <td>
                                        <a href="{{ route('clientevents.attendees',[$event->uuid]) }}">{{$event['title']}}</a>
                                    </td>

                                    <td>
                                        {{($event->attendees->count()) ?? 'N/A'}}
                                    </td>
                                    <td>
                                        {{($event->start_date)? $event->start_date->format('d M ,Y') : 'N/A'}}
                                    </td>
                                    <td>
                                        {{($event->end_date)? $event->end_date->format('d M ,Y') : 'N/A'}}
                                    </td>


                                @endforeach
                            </tbody>
                        </table>
        	</div>
        </div>
    </div>
</div>
@endsection

@push('head')
<link href="{{ asset('plugins/summernote/dist/summernote.css') }}" rel="stylesheet"/>
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@push('scripts')
<script src="{{ asset('plugins/summernote/dist/summernote.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{ asset('plugins//jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.keyTable.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/responsive.bootstrap.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.scroller.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.fixedHeader.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('.event-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
</script>
@endpush
