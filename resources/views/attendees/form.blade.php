{{csrf_field()}}
<div class="row">
    <div class="col-md-6">
        @include('partials.event-select')
        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
            <label class=" control-label" for="full_name">
                Full Name
            </label>
            <input autofocus class="form-control" id="full_name" name="full_name" placeholder="Full Name" type="text" value="{{old('title',$attendee->full_name ?? null )}}"/>
            @if ($errors->has('full_name'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('full_name') }}
                </strong>
            </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label class=" control-label" for="phone">
                Phone Number
            </label>
            <input autofocus="" class="form-control" id="phone" name="phone" placeholder="Phone Number" type="tel" value="{{ old('phone',$attendee->phone ?? null ) }}"/>
            @if ($errors->has('phone'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('phone') }}
                </strong>
            </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class=" control-label" for="email">
                Email
            </label>
            <input autofocus="" class="form-control" id="email" name="email" placeholder="Email Address" type="email" value="{{ old('email',$attendee->email ?? null ) }}"/>
            @if ($errors->has('email'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('email') }}
                </strong>
            </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
            <label class=" control-label" for="company">
                Company
            </label>
            <input autofocus="" class="form-control" id="company" name="company" placeholder="Company" type="text" value="{{ old('company',$attendee->company ?? null ) }}"/>
            @if ($errors->has('company'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('company') }}
                </strong>
            </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
            <label class=" control-label" for="position">
                Position
            </label>
            <input autofocus="" class="form-control" id="position" name="position" placeholder="Position" type="text" value="{{ old('position',$attendee->position ?? null ) }}"/>
            @if ($errors->has('position'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('position') }}
                </strong>
            </span>
            @endif
        </div>


    </div>

    <div class="col-md-6">
         @include('partials.lived-abroad-select')
        <div class="form-group{{ $errors->has('pin_code') ? ' has-error' : '' }}">
            <label class=" control-label" for="pin_code">
                Pin Code
            </label>
            <input autofocus="" class="form-control" id="pin_code" name="pin_code" readonly="true" placeholder="Pin Code" type="text" value="{{ old('pin_code',$pin_code ?? null ) }}"/>
            @if ($errors->has('pin_code'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('pin_code') }}
                </strong>
            </span>
            @endif
        </div>

         <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            <label class=" control-label" for="gender">
                Gender
            </label>
            @php
                $attendee_male_gender=((isset($attendee->gender) && $attendee->gender=='male') || !isset($attendee->gender) )? ' checked':'';
                $attendee_female_gender=((isset($attendee->gender)) && $attendee->gender=='female')? ' checked':'';
            @endphp
            <ul class="list-inline">
                <li>
            <input class="with-gap" id="gender_male" name="gender" type="radio" value="male" {{$attendee_male_gender}}/>
            <label for="gender_male">
                Male
            </label>
            </li>
            <li>
                <input class="with-gap" id="gender_female" name="gender" type="radio" value="female" {{$attendee_female_gender}} />
                <label class="m-l-20" for="gender_female">
                    Female
                </label>
            </li>
            </ul>
            <span class="help-block">
                <strong>
                    {{ $errors->first('gender') }}
                </strong>
            </span>
        </div>

        <div class="form-group{{ $errors->has('mode') ? ' has-error' : '' }}">
            <label class=" control-label" for="name">
                Mode
            </label>
            @php
                $attendee_live_mode=((isset($attendee->mode) && $attendee->mode=='live') || !isset($attendee->mode) )? ' checked':'';
                $attendee_test_mode=((isset($attendee->mode)) && $attendee->mode=='test')? ' checked':'';
            @endphp
            <ul class="list-inline">
                <li>
                    <input class="with-gap" id="mode_live" name="mode" type="radio" value="live" {{$attendee_live_mode}}/>
                    <label for="mode_live">
                        Live
                    </label>
                </li>
                 <li>
                    <input class="with-gap" id="mode_test" name="mode" type="radio" value="test" {{$attendee_test_mode}} />
                    <label class="m-l-20" for="mode_test">
                        Test
                    </label>
                    <span class="help-block">
                        <strong>
                            {{ $errors->first('mode') }}
                        </strong>
                    </span>
                </li>
            </ul>
        </div>

         <div class="form-group{{ $errors->has('registration') ? ' has-error' : '' }}">
            <label class=" control-label" for="name">
                Registration
            </label>
            @php
            $attendee_input =' checked';
                $attendee_pin=(isset($attendee->registration) || !isset($attendee->registration))? ' checked':'';
                $attendee_input=((isset($attendee->registration) && $attendee->registration=='during_event') )? ' checked':'';
            @endphp
            <ul class="list-inline">
            <li>
                <input class="with-gap" id="registration_pin" name="registration" type="radio" value="pin" {{$attendee_pin}} />
                <label  for="registration_pin">
                    Pin
                </label>

            </li>
            <li>
                <input class="with-gap" id="registration_input" name="registration" type="radio" value="input" {{$attendee_input}}/>
                <label for="registration_input">
                    Input Fields
                </label>
            </li>
            </ul>
            <span class="help-block">
                    <strong>
                        {{ $errors->first('registration') }}
                    </strong>
                </span>
        </div>

         <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label class=" control-label" for="name">
                Status
            </label>
            @php
                $attendee_active_status=((isset($attendee->status) && $attendee->status=='active') || !isset($attendee->status) )? ' checked':'';
                $attendee_inactive_status=((isset($attendee->status)) && $attendee->status=='inactive')? ' checked':'';
            @endphp
            <ul class="list-inline">
                <li>
            <input class="with-gap" id="status_active" name="status" type="radio" value="active" {{$attendee_active_status}}/>
            <label for="status_active">
                Active
            </label>
            </li>
            <li>
                <input class="with-gap" id="status_inactive" name="status" type="radio" value="inactive" {{$attendee_inactive_status}} />
                <label class="m-l-20" for="status_inactive">
                    Inactive
                </label>
            </li>
            </ul>
            <span class="help-block">
                <strong>
                    {{ $errors->first('status') }}
                </strong>
            </span>
        </div>



    </div>
</div>

<div class="form-group">
    <input class="btn btn-primary pull-right" name="submit" type="submit" value="Submit"/>
</div>

