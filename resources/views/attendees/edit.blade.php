@extends('layouts.app')
@section('title'){{'Attendees -  '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Attendees - Edit [#{{$event->id}}]
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
             <li>
                <a href="{{route('attendees.index')}}">
                    Attendees
                </a>
            </li>
            <li class="active">
                {{$attendee->full_name}}
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <form action="{{route('attendees.update',['id'=>$attendee->id])}}" class="" enctype="multipart/form-data" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            @include('attendees.form',['event_id'=>$event_id])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
