@extends('layouts.app')
@section('title'){{'Import Attendees - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Attendees - Import
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('attendees.index')}}">
                    Attendees
                </a>
            </li>
            <li class="active">
                Import
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <form action="{{route('attendees.storeimport')}}"  enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-6">
                                    @include('partials.event-select')
                                    <div class="form-group{{ $errors->has('mode') ? ' has-error' : '' }}">
                                        <label class=" control-label" for="name">
                                            Mode
                                        </label>
                                        @php
                                            $attendee_live_mode=((isset($attendee->mode) && $attendee->mode=='live') || !isset($attendee->mode) )? ' checked':'';
                                            $attendee_test_mode=((isset($attendee->mode)) && $attendee->mode=='test')? ' checked':'';
                                        @endphp
                                        <ul class="list-inline">
                                            <li>
                                                <input class="with-gap" id="mode_live" name="mode" type="radio" value="live" {{$attendee_live_mode}}/>
                                                <label for="mode_live">
                                                    Live
                                                </label>
                                            </li>
                                             <li>
                                                <input class="with-gap" id="mode_test" name="mode" type="radio" value="test" {{$attendee_test_mode}} />
                                                <label class="m-l-20" for="mode_test">
                                                    Test
                                                </label>
                                                <span class="help-block">
                                                    <strong>
                                                        {{ $errors->first('mode') }}
                                                    </strong>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>

                                <div class="form-group{{ $errors->has('registration') ? ' has-error' : '' }}">
                                    <label class=" control-label" for="name">
                                        Registration
                                    </label>
                                    @php
                                        $attendee_pin=(isset($attendee->registration) && $attendee->registration=='before_event' || !isset($attendee->registration))? ' checked':'';
                                        $attendee_input=((isset($attendee->registration) && $attendee->registration=='during_event') )? ' checked':'';
                                    @endphp
                                    <ul class="list-inline">
                                    <li>
                                        <input class="with-gap" id="registration_pin" name="registration" type="radio" value="pin" {{$attendee_pin}} />
                                        <label  for="registration_pin">
                                            Pin
                                        </label>
                                        
                                    </li>
                                    <li>
                                        <input class="with-gap" id="registration_input" name="registration" type="radio" value="input" {{$attendee_input}}/>
                                        <label for="registration_input">
                                            Input Fields
                                        </label>
                                    </li>
                                    </ul>
                                    <span class="help-block">
                                            <strong>
                                                {{ $errors->first('registration') }}
                                            </strong>
                                        </span>
                                </div>

                                 <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                    <label class=" control-label" for="name">
                                        Status
                                    </label>
                                    @php
                                        $attendee_active_status=((isset($attendee->status) && $attendee->status=='active') || !isset($attendee->status) )? ' checked':'';
                                        $attendee_inactive_status=((isset($attendee->status)) && $attendee->status=='inactive')? ' checked':'';
                                    @endphp
                                    <ul class="list-inline">
                                        <li>
                                    <input class="with-gap" id="status_active" name="status" type="radio" value="active" {{$attendee_active_status}}/>
                                    <label for="status_active">
                                        Active
                                    </label>
                                    </li>
                                    <li>
                                        <input class="with-gap" id="status_inactive" name="status" type="radio" value="inactive" {{$attendee_inactive_status}} />
                                        <label class="m-l-20" for="status_inactive">
                                            Inactive
                                        </label>
                                    </li>
                                    </ul>
                                    <span class="help-block">
                                        <strong>
                                            {{ $errors->first('status') }}
                                        </strong>
                                    </span>
                                </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="card-inner">
                                        <div class="form-group{{ $errors->has('spreadsheet_file') ? ' has-error' : '' }}">
                                            <label>Select Spreadsheet file</label>
                                            <input type="file" accept=".xls,.xlsx,.csv" class="dropify" name="spreadsheet_file" />
                                            <span class="help-block">
                                                <strong>
                                                    {{ $errors->first('spreadsheet_file') }}
                                                </strong>
                                            </span>
                                         </div>
                                    </div>
                                    
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="btn btn-primary pull-right" name="submit" type="submit" value="Submit"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('head')
<link href="{{ asset('plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">
@endpush
@push('scripts')
<script src="{{ asset('plugins/dropify/dist/js/dropify.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {
   $('.dropify').dropify();
});
</script>
@endpush