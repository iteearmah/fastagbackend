@extends('layouts.app')
@if($event)
    @section('title'){{$event->title.' - Attendees - '.config('app.name')}}@endsection
@else
    @section('title'){{'Attendees - '.config('app.name')}}@endsection
@endif

@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            @if($event)
                Attendees
            <a class="btn btn-primary" href="{{route('attendees.create',['event'=>$event->id])}}">
                <i class="fa fa-plus">
                </i>
                Add New
            </a>
            @else
                Attendees
            <a class="btn btn-primary" href="{{route('attendees.create')}}">
                <i class="fa fa-plus">
                </i>
                Add New
            </a>
            @endif
            @if($event)
            <a href="{{route('attendees.import',['event'=>$event->id])}}" class="btn btn-success"><i class="fa fa-upload"></i> Import</a>
            @endif
            @if($event)
            <a href="{{route('attendees.export',['event'=>$event->id])}}" class="btn btn-success"><i class="fa fa-file-excel-o"></i> Export</a>
            @endif
            @if($event)
            <a href="{{route('attendees.clearall',['event'=>$event->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete all {{$event->title}} attendees?');"><i class="fa fa-trash-o"></i> Clear All</a>
            @endif
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('attendees.index')}}">
                    Attendees
                </a>
            </li>
            @if($event)
            <li class="active">
                {{$event->title}}
            </li>
            @endif
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                @if($event)
                <div class="card-header">
                    <h2>
                        {{$event->title.' - Attendees'}}
                    </h2>
                </div>
                @endif
                <div class="body">
                    @include('flash::message')
                    <div class="row">
                        <!--Advanced Search-->
                        <div class="col-md-12">
                        </div>
                        <!--#Advanced Search-->
                        <div class="table table-responsive">
                            @php
                                $formFields=['full_name'=>'Full Name','phone'=>'Phone','email'=>'Email','company'=>'Company','position'=>'Position','gender'=>'Gender','age'=>'Age','location'=>'Location','country'=>'Country','level_education'=>'Highest Level of Education','lived_abroad'=>'Lived Abroad (6 months above)','attending_as'=>'Attending As'];
                            @endphp
                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>
                                        Pin Code
                                    </th>
                                    @foreach($formFields as $formKey =>$formLabel)
                                        @if($event['app_field_'.$formKey] ==='on')
                                            <th>{{ $formLabel }}</th>
                                        @endif
                                    @endforeach
                                    <th>
                                        Created
                                    </th>
                                    <th colspan="2">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($attendees as $attendee)
                                <tr>
                                    <td>
                                        {{$attendee->attendee_event->pin_code ?? ''}}
                                    </td>
                                    @foreach($formFields as $formKey =>$formLabel)
                                        @if($event['app_field_'.$formKey] ==='on')
                                            @if($formKey === 'country')
                                                @php
                                                    $countries = config('countries');
                                                @endphp
                                                <td>{{ (!empty($attendee[$formKey])) ? $countries[$attendee[$formKey]] : 'N/A'  }}</td>
                                            @else
                                                <td>{{ (!empty($attendee[$formKey]) &&  ($attendee[$formKey]!= 'null')) ? $attendee[$formKey] : 'N/A' }}</td>
                                            @endif
                                        @endif
                                    @endforeach
                                    <td>
                                        {{$attendee->created_at->format('d M ,Y h:ia')}}
                                    </td>
                                    @isset($attendee->attendee_event->pin_code)
                                    <td>
                                        <a class="btn btn-default" target="_blank" href="{{ asset(config('main.qr_code_dir') . '/' . $attendee->attendee_event->pin_code  .'.png') }}" title="View QR Code">
                                            <i class="fa fa-qrcode">
                                            </i>
                                        </a>
                                    </td>
                                    @endisset
                                    <td>
                                        <a class="btn btn-default" href="{{ route('attendees.show',$attendee->id) }}" title="View">
                                            <i class="fa fa-eye">
                                            </i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('attendees.edit',$attendee->id) }}" title="Edit">
                                            <i class="fa fa-edit">
                                            </i>
                                        </a>
                                    </td>
                                    {{--
                                    <td>
                                        <form action="{{ route('attendees.destroy',['id'=>$attendee->id]) }}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-danger" title="Delete" type="submit">
                                                    <i class="fa fa-trash">
                                                    </i>
                                                </button>
                                            </input>
                                        </form>
                                        --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        @if(!$event)
                        <div class="col-md-12">
                            Showing {{($attendees->currentpage()-1)* $attendees->perpage()+1}} to {{(($attendees->currentpage()-1) * $attendees->perpage())+$attendees->count()}} of {{$attendees->total()}} entries
                        </div>
                        <div class="pull-right">
                            {{ $attendees->links() }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
