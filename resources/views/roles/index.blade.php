@extends('layouts.app')
@section('title'){{'Roles - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Roles 
            <a href="{{route('roles.create')}}" class="btn btn-primary"><i class="fa fa-plus" ></i> Add New</a>  
            <a class="btn btn-default" href="{{ route('users.index') }}">Users</a>
            <a class="btn btn-default" href="{{ route('permissions.index') }}">Permissions</a>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li class="active">
                Roles
            </li>
        </ol>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if(Session::has('flash_message'))
                    <div class="alert alert-success">{!! session('flash_message') !!}</div>
                    @endif 
                    @include('errors.list')
                    <div class="table-responsive">
                         <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Role
                                    </th>
                                    <th >
                                        Permissions
                                    </th>
                                    <th>
                                        Operation
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)
                                <tr>
                                    <td>
                                        {{ $role->name }}
                                    </td>
                                    <td>
                                        <ul class="list-inline col-sm-12">
                                            @foreach ($role->permissions as $permission)
                                             <li>{{ucwords($permission->name)}}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    {{-- Retrieve array of permissions associated to a role and convert to string --}}
                                    <td>
                                        <div class="row">
                                        <div class="col-sm-6">
                                            <a class="btn btn-warning pull-left" href="{{ route('roles.edit',$role->id)}}" >
                                                <i class="fa fa-pencil">
                                                </i>
                                                Edit
                                            </a>
                                        </div>
                                        <div class="col-sm-6 ">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                                        </div>
                                    </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
