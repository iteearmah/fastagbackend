@extends('layouts.app')
@section('title'){{'Edit '.$role->name.' Role - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Edit Role: {{$role->name}}
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('roles.index')}}">
                    Roles
                </a>
            </li>
            <li class="active">
                Edit Role: {{$role->name}}
            </li>
        </ol>
    </div>

     <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                                @include('errors.list')
              {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Role Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <h5><b>Assign Permissions</b></h5>
    <ul class="list-inline">
        @php
        $role_permissions=$role->permissions->pluck('id')->toArray();
        @endphp
        @foreach ($role->permissions as $permission)
         <li class="eagle-checkbox">
                        <label class="eagle-check custom-checkbox">
             <input type="checkbox" name="permissions[]" class="eagle-check-input" value="{{ $permission['id']}}" {{(in_array($role['id'],$role_permissions))? 'checked':''}} />
            <span class="eagle-check-indicator"></span>
            <span class="eagle-check-description">{{ucfirst($permission->name)}}</span>
            </label>
            </li>
        @endforeach
    </ul>   
    {{ Form::submit('Edit', array('class' => 'btn btn-primary pull-right')) }}

    {{ Form::close() }} 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
