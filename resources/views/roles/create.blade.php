@extends('layouts.app')
@section('title'){{'Add Role - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
 <div class="page-header">
        <h2>
            Add Role
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('roles.index')}}">
                    Roles
                </a>
            </li>
            <li class="active">
                Add Role
            </li>
        </ol>
    </div>

     <div class="row clearfix">
        <div class="col-md-6">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
                                @include('errors.list')
              {{ Form::open(array('route' => 'roles.store')) }}
                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
                </div>
                <h5>
                    <b>
                        Assign Permissions
                    </b>
                </h5>
                <div class="form-group">
                    <ul class="list-inline">
                        @foreach ($permissions as $permission)
                         <li class="eagle-checkbox">
                                        <label class="eagle-check custom-checkbox">
                             <input type="checkbox" name="permissions[]" class="eagle-check-input" value="{{ $permission['id']}}" />
                            <span class="eagle-check-indicator"></span>
                            <span class="eagle-check-description">{{ucfirst($permission->name)}}</span>
                            </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                {{ Form::submit('Add', array('class' => 'btn btn-primary  pull-right')) }}

    {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
