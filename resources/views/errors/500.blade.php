@extends('layouts.error')
@section('title')
{{'403'}}
@endsection
@section('content')
<body class="error error_five">
    <div class="error-box">
        <img src="{{ asset('assets/images/error500.gif') }}" class="img-responsive center-block height150" alt="403">
        <div class="error-message">
           Whoops, looks like something went wrong.
           <b>Internal Server Error
        </div>
        <div class="error-bottom">
            <a href="{{ route('dashboard')}}" class="btn btn-primary">Hey, Take Me Home</a>
            {{-- <div class="report-error">
                or <a href="#">Report Issue</a>
            </div> --}}
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>
</body>
@endsection

