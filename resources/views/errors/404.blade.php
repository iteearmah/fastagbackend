@extends('layouts.error')
@section('title')
{{'404'}}
@endsection
@section('content')
<body class="error error_four">
    <div class="error-box">
        <img src="{{ asset('assets/images/logo-circle.png') }}" class="img-responsive center-block" alt="404">
        <div class="error-message">
            Oops! Page Not Found
        </div>
        <div class="error-bottom">
            <a href="{{ route('dashboard')}}" class="btn btn-primary">Hey, Take Me Home</a>
            {{-- <div class="report-error">
                or <a href="#">Report Issue</a>
            </div> --}}
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>
</body>
@endsection
