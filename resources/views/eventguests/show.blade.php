@extends('layouts.app')
@section('title'){{$attendee->full_name.' - Attendee - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Attendees - View [#{{$attendee->id}}]
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
             <li>
                <a href="{{route('attendees.index')}}">
                    Attendees
                </a>
            </li>
            <li class="active">
                {{$attendee->full_name}}
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-md-6">
                            <dl class="dl-vertical showview">
                                <dt>
                                    Event
                                </dt>
                                <dd>
                                    {{$attendee->Event->title}}
                                </dd>
                               
                                <dt>
                                    Full Name
                                </dt>
                                <dd>
                                    {{$attendee->full_name}}
                                </dd>
                                <dt>
                                    Phone Number
                                </dt>
                                <dd>
                                    {{$attendee['phone'] ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Email
                                </dt>
                                <dd>
                                    {{$attendee['email'] ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Company
                                </dt>
                                <dd>
                                    {{$attendee['company'] ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Position
                                </dt>
                                <dd>
                                    {{$attendee['position'] ?? 'N/A'}}
                                </dd>
                                
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-vertical showview">
                                <dt>
                                    Pin Code
                                </dt>
                                <dd>
                                    {{$attendee->attendee_event->pin_code ?? 'N/A'}}
                                </dd>
                                 <dt>
                                    Mode
                                </dt>
                                <dd>
                                    {{ucwords($attendee['mode']) ?? 'N/A'}}
                                </dd>
                                 <dt>
                                    Registration
                                </dt>
                                <dd>
                                    {{ucwords(str_replace('_', ' ', $attendee['registration'])) ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Status
                                </dt>
                                <dd>
                                    {{ucwords($attendee['status']) ?? 'N/A'}}
                                </dd>
                                <dt>
                                    UUID
                                </dt>
                                <dd>
                                    {{ucwords($attendee['uuid']) ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Created
                                </dt>
                                <dd>
                                    {{$attendee->created_at->format('j, M Y')}}
                                </dd>
                            </dl>
                        </div>
                        
                        <div class="col-md-12">
                           
                            <dl class="dl-vertical showview">
                                <dt>
                                    Attendance
                                </dt>
                                <dd>
                                    @if($attendee->Attendance)
                                    <table class="table">
                                        <tr>
                                            <th>Day</th>
                                            <th>Clocked In Date</th>
                                            <th>Clocked In Time</th> 
                                        </tr>
                                        @foreach($attendee->Attendance as $attendance)
                                            <tr>
                                                <th>{{$loop->index+1}}</th>
                                                <th>{{Carbon\Carbon::parse($attendance->clocked_in_date)->format('dS F,Y')}}</th>
                                                <th>{{Carbon\Carbon::parse($attendance->clocked_in_time)->format('g:i A')}}</th> 
                                            </tr>
                                        @endforeach
                                    </table>
                                    @endif
                                </dd>
                            </dl>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection