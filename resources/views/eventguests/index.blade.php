@extends('layouts.app')
@if($event)
    @section('title'){{$event->title.' - Event Guests - '.config('app.name')}}@endsection
@else
    @section('title'){{'Event Guests - '.config('app.name')}}@endsection
@endif

@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            @if($event)
                Event Guests
            <a class="btn btn-primary" href="{{route('eventguest.create',['event'=>$event->id])}}">
                <i class="fa fa-plus">
                </i>
                Add New
            </a>
            <a class="btn btn-success" href="{{route('eventguest.import',['event'=>$event->id])}}">
                <i class="fa fa-upload">
                </i>
                Import
            </a>
            @else
                Event Guests
            <a class="btn btn-primary" href="{{route('eventguest.create')}}">
                <i class="fa fa-plus">
                </i>
                Add New
            </a>
            <a class="btn btn-primary" href="{{route('eventguest.import')}}">
                <i class="fa fa-file-import">
                </i>
                Import
            </a>
            @endif
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li>
                <a href="{{route('eventguest.index')}}">
                    Event Guests
                </a>
            </li>
            @if($event)
            <li class="active">
                {{$event->title}}
            </li>
            @endif
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                @if($event)
                <div class="card-header">
                    <h2>
                        <a href="{{route('events.show',$event['id'])}}">{{$event->title}}</a>  {{' - Event Guests'}}
                    </h2>
                </div>
                @endif
                <div class="body">
                    @include('flash::message')
                    <div class="row">
                        <!--Advanced Search-->
                        <div class="col-md-12">
                        </div>
                        <!--#Advanced Search-->
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Table Number
                                    </th>
                                    <th>
                                        No. of Seats on Table
                                    </th>
                                    <th>
                                        Type
                                    </th>
                                    <th>
                                        Created
                                    </th>
                                    <th colspan="2">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($eventguests as $eventguest)
                                <tr>

                                    <td>
                                        {{$eventguest['name']}}
                                    </td>
                                    <td>
                                        {{$eventguest['table_number']}}
                                    </td>
                                    <td>
                                        {{$eventguest['seats_on_table']}}
                                    </td>
                                    <td>
                                        {{$eventguest['type']}}
                                    </td>
                                    <td>
                                        {{$eventguest->created_at->format('d M ,Y h:ia')}}
                                    </td>
                                    <td>
                                        <a class="btn btn-default" href="{{ route('eventguest.show',$eventguest['id']) }}" title="View">
                                            <i class="fa fa-eye">
                                            </i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('eventguest.edit',$eventguest['id']) }}" title="Edit">
                                            <i class="fa fa-edit">
                                            </i>
                                        </a>
                                    </td>

                                    <td>
                                        <form action="{{ route('eventguest.destroy',['id'=>$eventguest['id']]) }}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                                <button class="btn btn-danger" title="Delete" type="submit">
                                                    <i class="fa fa-trash">
                                                    </i>
                                                </button>
                                            </input>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if(!$event)
                        <div class="col-md-12">
                            Showing {{($eventguests->currentpage()-1)* $eventguests->perpage()+1}} to {{(($eventguests->currentpage()-1) * $eventguests->perpage())+$eventguests->count()}} of {{$eventguests->total()}} entries
                        </div>
                        <div class="pull-right">
                            {{ $eventguests->links() }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
