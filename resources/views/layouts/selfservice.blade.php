<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#FCA906" />
    <title>Event Form {{ config('app.name') }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{ asset('assets/images/apple-touch-icon-ipad.png') }}">
<link href="{{ asset('css/app.css?v1.0.37') }}" rel="stylesheet">
    <style type="text/css">

        body{
            background: url({{ asset('assets/images/tabbg-min.png') }}) no-repeat center top 70px fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            caret-color: #FCA906;
            background-color: #b17cae;
        }
        .container{
            padding: 0px !important;
        }
        .event-image-box{
            padding: 10px 0;
            background-color: #fff;
            text-align: center;
        }
        .event-form{
            padding: 20px 30px 0;
        }
        .event-image img{
            max-height: 150px !important;
        }
        .event-form label{
            color: #fff;
        }
        .field-checkbox{
            padding-left: 0px !important;
        }
        .field-box
        {
            margin-top: -13px;
            position: relative;
        }
       .field-box input{
           border: 1px solid #fff;
           border-radius: 1.25rem;
           height: 45px;
           text-indent: 40px;
       }
        .field-box input:focus {
            color: #495057;
            background-color: #fff;
            border-color: #625b6f;
            outline: 0;
            -webkit-box-shadow: 0 0 0 0.2rem rgb(98 91 111);
            box-shadow: 0 0 0 0.3rem rgb(98 91 111);
        }
        .field-box label{
            color: #fff;
            margin: 10px 0 -10px 10px;
            text-shadow: 0 0.05rem rgb(98 91 111);
        }
       .field-icon{
           position: absolute;
           left: 8px;
           top: 38px;
           width: 35px;
           height: 35px;
       }
       .clear-all-btn-box{
           width: 42% !important;
           float: left;
           text-align: center;
       }
       .submit-btn-box{
           width: 56% !important;
           display: flex;
           float: right;
           text-align: center;
       }
        .clear-all-btn-box,.submit-btn-box{
            text-align: center;
        }
        .clear-all-btn-box button,.submit-btn-box button
        {
            font-weight: bold;
            padding: 10px;
        }
        .bottom-margin{
            margin-bottom: 20px;
        }
        .modal-body{
            font-size: medium;
            margin: inherit;
        }
        #welcome_message{
            font-size: 40px;
            color: #ffffff;
        }
        #welcome_message_full_name
        {
            font-size: 43px;
            color: #ffffff;
            font-weight: bold;
        }
        #welcome_message_event{
            font-size: 23px;
            color: #F6BC09;
            font-weight: bold;
        }
        #welcome_message_full_name,#welcome_message_event,#welcome_message
        {
            text-align: center;
            text-shadow: 0 0.05rem rgb(98 91 111);
        }

        .container {
            height: 400px;
            position: relative;
        }

        .vertical-center {
            margin: 0;
            position: relative;
            top: 40%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }
        .full-width{
            width: 100% !important;
        }
    </style>
</head>
<body>
<div id="app">
    @yield('content')
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>

