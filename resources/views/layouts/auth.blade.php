<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'FASTAG') }}</title>

    <!-- Favicon-->
    <link rel="icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">

    <!-- Google Fonts -->
   <link href="{{ asset('assets/google-fonts/google-fonts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/material-design-icons/material-icons.css') }}" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/material-design-icons/material-icons.css') }}" rel="stylesheet">
    @if(Request::server ("HTTP_HOST") == config('main.client_domain'))
        <style type="text/css">
            body{background-image:  url({{asset('photos/african_symbols2.jpg')}}) !important;; background-color: #fff !important; background-repeat: repeat}
        </style>
    @endif
</head>

<body class="login-page">
    <div class="login-box">
        <div class="card">
           <div class="body">
    <div class="row">
        <div class="col-lg-12">
            <div class="login-logo">
                <img src="{{ asset('assets/images/logo-circle.png') }}" alt="" class="img-responsive align-center">
            </div>
        </div>
    </div>
@if (session('message'))
 <div class="alert alert-success">
   {{ session('message') }}
 </div>
@endif
     @yield('content')

    </div>
        </div>
    </div>

    <!-- CORE PLUGIN JS -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!--THIS PAGE LEVEL JS-->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/js/pages/examples/login.js') }}"></script>

    <!-- LAYOUT JS -->
    <script src="{{ asset('assets/js/demo.js') }}"></script>

</body>

</html>
