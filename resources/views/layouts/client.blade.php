<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title',config('app.name', 'FASTAG'))</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.png" type="image/x-icon">

    <!--REQUIRED PLUGIN CSS-->
    <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/spinkit/spinkit.css') }}" rel="stylesheet">

    <!--REQUIRED THEME CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/layout.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/main_theme.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/google-fonts/google-fonts.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/material-design-icons/material-icons.css') }}" rel="stylesheet">
    <!--THIS PAGE LEVEL CSS-->
    <link href="{{ asset('plugins/unslider/css/unslider.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/chartist/css/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/file-input/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2-bootstrap.min.css') }}" rel="stylesheet">
    <!--Chat Css-->
    <link href="{{ asset('plugins/wchat/assets/css/style-light.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/wchat/assets/css/mobile.css') }}" rel="stylesheet" id="style">

    <!-- EMOJI ONE JS -->
    <link rel="stylesheet" href="{{ asset('plugins/wchat/smiley/assets/sprites/emojione.sprites.css') }}"/>
    <script src="{{ asset('plugins/wchat/smiley/js/emojione.min.js') }}"></script>
    @stack('head')
    <script type="text/javascript">
        // #################################################
        // # Optional

        // default is PNG but you may also use SVG
        emojione.imageType = 'png';
        emojione.sprites = false;

        // default is ignore ASCII smileys like :) but you can easily turn them on
        emojione.ascii = true;

        // if you want to host the images somewhere else
        // you can easily change the default paths
        emojione.imagePathPNG = '{{ asset('plugins/wchat/smiley/assets/png/') }}';
        emojione.imagePathSVG = '{{ asset('plugins/wchat/smiley/assets/svg/') }}';

        // #################################################
    </script>
    <style type="text/css">
    body{margin: 0px; padding: 0px;}
        .theme-pink .topnavbar .nav-wrapper {
             background-color: #A32F54;
        }
        .content-wrapper {
            padding: 0px;
        }
        .content-wrapper .container, .content-wrapper .container-fluid {
            padding-right: 15px;
            padding-left: 15px;
            padding-top:30px;
            padding-bottom:30px;
            margin-right: auto;
            margin-left: auto;
        }
        .page-header{margin-left: 8px;}
        .breadcrumb li a {
            color: #e91e63;
        }
        .breadcrumb > li + li:before {
            color: #e91e63;
        }
        .showview dd{ margin-top: 5px }
        .showview dt{ font-size: 18px; margin-top: 10px }
        .colorpicker{z-index: 2}
        .dropify-wrapper .dropify-message p {
            text-align: center;
        }
        .nav-tabs.nav-tabs-noborder > li.active a {
            background-color: #FF2D7F !important;
        }
        .switch label .lever {
    margin: 0 2px;
}
    </style>
    <!--#End# Chat Css-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="theme-pink light layout-fixed aside-collapsed">
<div class="wrapper">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="sk-wave">
                <div class="sk-rect sk-rect1 bg-pink"></div>
                <div class="sk-rect sk-rect2 bg-pink"></div>
                <div class="sk-rect sk-rect3 bg-pink"></div>
                <div class="sk-rect sk-rect4 bg-pink"></div>
                <div class="sk-rect sk-rect5 bg-pink"></div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- top navbar-->
    <header class="topnavbar-wrapper">
        <nav role="navigation" class="navbar topnavbar">
            <!-- START navbar header-->
            <div class="navbar-header">
                <a href="./" class="navbar-brand">
                    <div class="brand-logo">
                        <img src="{{ asset('assets/images/logo.png') }}" alt="Admin Logo" class="img-responsive">
                    </div>
                    <div class="brand-logo-collapsed">
                        <img src="{{ asset('assets/images/logo-circle.png') }}" alt="Admin Logo" class="img-responsive">
                    </div>
                </a>
            </div>
            <!-- END navbar header-->
            <!-- START Nav wrapper-->
            <div class="nav-wrapper">
                <!-- START Left navbar-->
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" class="hidden-xs">
                            <em class="material-icons">menu</em>
                        </a>
                        <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" class="visible-xs sidebar-toggle">
                            <em class="material-icons">menu</em>
                        </a>
                    </li>
                </ul>
                <!-- END Left navbar-->
                <!-- START Right Navbar-->
                <ul class="nav navbar-nav navbar-right">


                      <li class=""><a href=""><i class="fa fa-user-circle-o fa-1" ></i> {{Auth::user()->name}}</a></li>
                       <li class="">
                           <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i>  Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                            </li>

                             <li class="visible-lg">
                        <a href="#" data-toggle-fullscreen="" title="Fullscreen">
                            <em class="material-icons">fullscreen</em>
                        </a>
                    </li>

                </ul>
                <!-- #END# Right Navbar-->
            </div>
            <!-- #END# Nav wrapper-->
        </nav>
        <!-- END Top Navbar-->
    </header>
    <!-- sidebar-->
    <aside class="aside" style="background-color: #000">
        <!-- START Sidebar (left)-->
        <div class="aside-inner">
            <nav data-sidebar-anyclick-close="" class="sidebar">
                <!-- START sidebar nav-->
                <ul class="nav menu">
                    <!-- Iterates over all sidebar items-->
                    <li class="nav-heading ">
                        <span>MAIN NAVIGATION</span>
                    </li>
                    <li {{{ (Request::is('dashboard') ? 'class=active' : '') }}}>
                        <a href="{{route('dashboard')}}" title="Dashboard" >
                            <em class="material-icons">dashboard</em>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li {{{ (Request::is('clientevents.index') ? 'class=active' : '') }}}>
                        <a href="{{route('clientevents.index')}}" title="Events" >
                            <em class="material-icons">event</em>
                            <span>Events</span>
                        </a>
                    </li>
                </ul>
                <!-- END sidebar nav-->
            </nav>
        </div>
        <!-- #END# Sidebar (left)-->
    </aside>

    <!-- Main section-->
    <section>
        <!-- Page content-->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- END Page content-->
    </section>
    <!-- FOOTER-->
    <footer>
        <span>&copy; {{\Carbon\Carbon::now()->format('Y')}} - <b class="col-blue">{{config('app.name')}}</b></span>
    </footer>
</div>
    <!-- CORE PLUGIN JS -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/modernizr/modernizr.custom.js') }}"></script>
    <script src="{{ asset('plugins/screenfull/dist/screenfull.js') }}"></script>
    <script src="{{ asset('plugins/jQuery-Storage-API/jquery.storageapi.js') }}"></script>
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!--THIS PAGE LEVEL JS-->
    <script src="{{ asset('plugins/masonry/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('plugins/skycon/skycons.js') }}"></script>
    <script src="{{ asset('plugins/masonry/masonry.min.js') }}"></script>
    <script src="{{ asset('plugins/unslider/js/unslider-min.js') }}"></script>
    <script src="{{ asset('plugins/file-input/js/jasny-bootstrap.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <!-- #End# Chat js-->



    <!-- LAYOUT JS -->
    <script src="{{ asset('assets/js/demo.js') }}"></script>
    <script src="{{ asset('assets/js/layout.js') }}"></script>

    @stack('scripts')
</body>

</html>
