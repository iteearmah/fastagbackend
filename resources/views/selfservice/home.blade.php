@extends('layouts.selfservice')
@section('content')
    <div class="justify-content-center event-image-box" >
        <div class="col-md-12 event-image">
            <img src="{{ asset('assets/images/logo.png') }}" class="img-fluid">
        </div>
    </div>
    <div class="container">
        <div class="event-form-box d-flex">
            <h1 class="mt-5 align-content-center flex-fill">{{ __('Welcome to the Enclave Africa Systems Registration Online Platform') }}</h1>
        </div>
    </div>
@endsection
