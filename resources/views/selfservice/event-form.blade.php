@extends('layouts.selfservice')
@section('content')
    @if($event->cover_photo)
    <div class="justify-content-center event-image-box" >
        <div class="col-md-12 event-image">
            <img src="{{ asset(config('main.cover_photo_dir') . '/' . $event->cover_photo) }}" class="img-fluid">
        </div>
    </div>
    @endif
    <div class="container">
        @if($event)
        <div class="event-form-box">
            <selfcare-event-form-component :event_props="{{ json_encode([ 'event' => $event, 'mode' => $mode, 'event_title' => $event->title, 'event_code' => $event->event_code, 'apiToken' => $loginDetails['token']]) }}" ></selfcare-event-form-component>
        </div>
        @else

        @endif
    </div>
@endsection
