@extends('layouts.auth')

@section('content')
<center><h4>{{ __('Reset Password') }}</h4></center>
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
    @csrf

    <div class="form-group">
            <label class=" control-label" for="event_title">
                {{ __('E-Mail Address') }}
            </label>
           <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

    <div class="form-group row">
        <div class="col-md-6 offset-md-4 ">
            <button type="submit" class="btn btn-primary">
                {{ __('Send Password Reset Link') }}
            </button>
        </div>
    </div>
</form>
</div>

@endsection
