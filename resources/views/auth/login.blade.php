@extends('layouts.auth')

@section('content')

    <form id="log_in" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
                @if ($errors->has('email') || $errors->has('username'))
                    <div class="alert dark alert-danger alert-dismissible">{{ $errors->first('username') }} {{ $errors->first('email') }}</div>
                @endif
        <div class="input-group addon-line ">
            <span class="input-group-addon">
                <i class="material-icons">person</i>
            </span>
            <div class="form-line {{ $errors->has('email') ? ' has-error' : '' }}">
                <!-- <input type="text" class="form-control" name="username" placeholder="Username" required autofocus> -->
                <input id="email" type="text" class="form-control" name="email"  placeholder="Username" value="{{ old('email') }}" required autofocus>
              
            </div>
                
        </div>
      
        <div class="input-group addon-line">
            <span class="input-group-addon">
                <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
                <!-- <input type="password" class="form-control" name="password" placeholder="Password" required> -->
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required autofocus>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 p-t-5">
                <!-- <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-blue"> -->
                <input type="checkbox" class="filled-in chk-col-blue" name="remember" id="rememberme" {{ old('remember') ? 'checked' : '' }}> 
                <label for="rememberme">Remember Me</label>
            </div>
            <div class="col-xs-6 align-right p-t-5">
                <a href="{{ route('password.request') }}">Forgot Password?</a>
            </div>
        </div>

        <button class="btn btn-block btn-primary waves-effect" type="submit">LOG IN</button>

        <!-- <p class="text-muted text-center p-t-20">
            <small>Do not have an account?</small>
        </p>

        <a class="btn btn-sm btn-default btn-block" href="register.html">Create an account</a> -->

    </form>
    
@endsection