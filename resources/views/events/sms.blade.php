@extends('layouts.app')
@section('title'){{'Send Event SMS - '.$event->title.' - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Send Event SMS - [{{$event->title}}]
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
             <li>
                <a href="{{route('events.index')}}">
                    Send Event SMS
                </a>
            </li>
            <li class="active">
                {{$event->title}}
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="clearfix">
                            <ul class="list-inline">
                                <li> <a class="btn btn-default" href="{{ route('events.show',$event->id) }}">
                                        <i class="fa fa-eye"></i> Event Details
                                    </a>
                                </li>
                                <li>
                                    <a class="btn btn-success " href="{{route('promopage',$event->uuid)}}" target="_blank">
                                        <i class="fa fa-external-link"></i> Goto Promo Page
                                    </a>
                                </li>
                            </ul>
                           
                            
                        </div>
                        <div class="col-md-6">
                            <dl class="dl-vertical showview">
                                <dt>
                                    Attendee Total
                                </dt>
                                <dd>
                                    {{($event->attendee_event->count()) ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Organizer
                                </dt>
                                <dd>
                                    {{$event->Organizer->name}}
                                </dd>
                               
                            </dl>
                        </div>
                        <div class="col-md-6">
                            <form action="{{route('events.sendsms',['id'=>$event->id])}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group{{ $errors->has('sms_message') ? ' has-error' : '' }}">
                                <label class=" control-label" for="event_sms_message">
                                    SMS Message
                                </label>
                                <textarea  class="form-control" id="event_sms_message" name="sms_message" placeholder="Event SMS Message" rows="5">{{ old('sms_message',$event->sms_message ?? null ) }}</textarea>
                                 <small>{!! __('Use <code>[EVENT_TITLE]</code> code as Event Title value, <code>[EVENT_LINK]</code> code as Event Page Link value and <code>[ATTENDEE_PIN_CODE]</code> code as Attendee PIN Code ') !!}</small>
                                <small><strong>Hint:</strong> The square bracket blocks will be automatically replaced with the appropraite content when SMS is sent.</small>
                                @if ($errors->has('excerpt'))
                                <span class="help-block">
                                    <strong>
                                        {{ $errors->first('sms_message') }}
                                    </strong>
                                </span>
                                @endif
                            </div>
                            
                             <div class="form-group">
                                <input class="btn btn-primary pull-right" name="submit" type="submit" value="Send"/>
                            </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
