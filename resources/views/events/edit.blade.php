@extends('layouts.app')
@section('title'){{$event->title.' - Events -  '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Event - Edit [{{$event->title}}]
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
             <li>
                <a href="{{route('events.index')}}">
                    Events
                </a>
            </li>
            <li class="active">
                {{$event->title}}
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <form action="{{route('events.update',['id'=>$event->id])}}" class="" enctype="multipart/form-data" method="POST">
                            <input type="hidden" name="_method" value="PUT">
                            @include('events.form',['organizer_id'=>$event->organizer])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
