{{csrf_field()}}
<ul class="nav nav-tabs nav-tabs-noborder nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#event-details" data-toggle="tab">EVENT DETAILS</a></li>
    <li role="presentation"><a href="#app-settings" data-toggle="tab">APP SETTINGS</a></li>
    <li role="presentation"><a href="#sms-settings" data-toggle="tab">SMS SETTINGS</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade in active" id="event-details" role="tabpanel">
            <div class="col-md-6">
                 @include('partials.organizer-select')

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            <label class=" control-label" for="event_title">
                Title
            </label>
            <input autofocus="" class="form-control" id="event_title" name="title" placeholder="Event Title" type="text" value="{{ old('title',$event->title ?? null ) }}"/>
            @if ($errors->has('title'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('title') }}
                </strong>
            </span>
            @endif
        </div>


        <div class="form-group{{ $errors->has('theme') ? ' has-error' : '' }}">
            <label class=" control-label" for="event_theme">
                Theme
            </label>
            <input  class="form-control" id="event_theme" name="theme" placeholder="Event Theme" type="text" value="{{ old('theme',$event->theme ?? null ) }}"/>
            @if ($errors->has('theme'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('theme') }}
                </strong>
            </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('venue') ? ' has-error' : '' }}">
            <label class=" control-label" for="event_venue">
                Venue
            </label>
            <input class="form-control" id="event_venue" name="venue" placeholder="Event Venue" type="text" value="{{ old('venue',$event->venue ?? null ) }}"/>
            @if ($errors->has('venue'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('venue') }}
                </strong>
            </span>
            @endif
        </div>

          <div class="row">

            <div class="col-md-6" style="padding-left: 9px">
                <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                    <label class=" control-label" for="event_start_date">
                        Start Date
                    </label>
                    <div class="input-group focused">
                        <input type="text" class="form-control " id="start_date" data-provide="datepicker" data-date-format="dd/mm/yyyy" name="start_date" value="{{ old('start_date',(isset($event->start_date))? $event->start_date->format('d/m/Y'): null ) }}" readonly>
                        <span class="input-group-addon bg"><i class="fa fa-calendar"></i></span>
                    </div>

                    @if ($errors->has('start_date'))
                    <span class="help-block">
                        <strong>
                            {{ $errors->first('start_date') }}
                        </strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-md-6" style="padding-left: 9px">
                <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                    <label class=" control-label" for="event_start_date">
                        End Date
                    </label>
                    <div class="input-group focused">
                        <input type="text" class="form-control" id="end_date" data-provide="datepicker" data-date-format="dd/mm/yyyy" name="end_date" value="{{ old('end_date',(isset($event->end_date))? $event->end_date->format('d/m/Y'): null ) }}" readonly>
                        <span class="input-group-addon bg"><i class="fa fa-calendar"></i></span>
                    </div>

                    @if ($errors->has('end_date'))
                    <span class="help-block">
                        <strong>
                            {{ $errors->first('end_date') }}
                        </strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group{{ $errors->has('programme_file') ? ' has-error' : '' }}">

            <div class="fileinput fileinput-new" data-provides="fileinput">
                @if(isset($event->programme_file))
                   <p><i class="fa fa-file"></i> <a href="{{ asset(config('main.programme_dir').'/'.$event->programme_file )}}">View Programme File</a> </p>
                @endif
                <span class="btn btn-info btn-file">
                    <span class="fileinput-new">
                        Select Programme File
                    </span>
                    <span class="fileinput-exists">
                        Change
                    </span>
                    <input name="programme_file" type="file">
                    </input>
                </span>
                <span class="fileinput-filename">
                </span>
                <a class="close fileinput-exists" data-dismiss="fileinput" href="#" style="float: none">
                    ×
                </a>
            </div>
        </div>
        <div class="form-group{{ $errors->has('programme_file_url') ? ' has-error' : '' }}">
            <label class=" control-label" for="programme_file_url">
                Programme File URL
            </label>
            <input class="form-control" id="programme_file_url" name="programme_file_url" placeholder="e.g. http://bitly.com/wewz2x" type="text" value="{{ old('programme_file_url',$event->programme_file_url ?? null ) }}"/>
            @if ($errors->has('title'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('programme_file_url') }}
                </strong>
            </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('excerpt') ? ' has-error' : '' }}">

            <label class=" control-label" for="event_excerpt">
                Excerpt
            </label>
            <textarea  class="form-control" id="event_excerpt" name="excerpt" placeholder="Event Excerpt">{{ old('excerpt',$event->excerpt ?? null ) }}</textarea>
            @if ($errors->has('excerpt'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('excerpt') }}
                </strong>
            </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label class=" control-label" for="name">
                Status
            </label>
            @php
                $organizer_active_status=((isset($organizer->status) && $organizer->status=='active') || !isset($organizer->status) )? ' checked':'';
                $organizer_inactive_status=((isset($organizer->status)) && $organizer->status=='inactive')? ' checked':'';
            @endphp
            <input class="with-gap" id="status_active" name="status" type="radio" value="active" {{$organizer_active_status}}/>
            <label for="status_active">
                Active
            </label>
            <input class="with-gap" id="status_inactive" name="status" type="radio" value="inactive" {{$organizer_inactive_status}} />
            <label class="m-l-20" for="status_inactive">
                Inactive
            </label>
            <span class="help-block">
                <strong>
                    {{ $errors->first('status') }}
                </strong>
            </span>
        </div>

        <!-- <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
            <label class=" control-label" for="event_details">
                Details
            </label>
            <textarea id="event_details" name="event_details">{{$event->details ?? null}}</textarea>
            @if ($errors->has('excerpt'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('details') }}
                </strong>
            </span>
            @endif
        </div> -->

    </div>
    </div>
    <div class="tab-pane fade" id="app-settings" role="tabpanel">
        <div class="col-md-4">


        <div class="form-group{{ $errors->has('background_color') ? ' has-error' : '' }}">
            <label class=" control-label" for="background_color">
                Background Color
            </label>
            <div class="input-group colorpicker colorpicker-element focused">
                <input type="text" id="hex_code" class="form-control"  name="background_color" placeholder="Event Promo Page Background"  value="{{ old('background_color',$event->background_color ?? '#A32F54' ) }}">
                <span class="input-group-addon"><i style="background-color: {{ old('background_color',$event->background_color ?? '#A32F54' ) }};"></i></span>
            </div>
            @if ($errors->has('background_color'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('background_color') }}
                </strong>
            </span>
            @endif
        </div>

        <div class="form-group{{ $errors->has('font_color') ? ' has-error' : '' }}">
            <label class=" control-label" for="font_color">
                Font Color
            </label>
            <div class="input-group colorpicker colorpicker-element focused">
                <input type="text" id="hex_code" class="form-control"  name="font_color" placeholder="Event Promo Page Font Color"  value="{{ old('font_color',$event->font_color ?? '#ffffff' ) }}">
                <span class="input-group-addon"><i style="background-color: {{ old('background_color',$event->font_color ?? '#ffffff' ) }};"></i></span>
            </div>
            @if ($errors->has('font_color'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('font_color') }}
                </strong>
            </span>
            @endif
        </div>

         <div class="form-group{{ $errors->has('cover_photo') ? ' has-error' : '' }} ">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail col-md-12" data-trigger="fileinput" style="min-width: 200px; ">
                     @if(isset($event->cover_photo))
                    <img alt="Logo" height="auto" id="photo_preview" onerror="this.src='{{asset('photos/nophoto.jpg')}}'" src="{{ asset(config('main.cover_photo_dir').'/medium.'.$event->cover_photo )}}"  />
                    @endif

                </div>
                <div>
                    <span class="btn btn-info btn-file"><span class="fileinput-new">Select Cover Photo</span>
                    <span class="fileinput-exists">Change</span><input accept="image/gif, image/jpg, image/jpeg, image/png" class="form-control" name="cover_photo"  type="file"></span>
                    <a href="#" class="btn btn-default bg-red fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
                @if ($errors->has('cover_photo'))
                    <span class="help-block">
                        <strong>
                            {{ $errors->first('cover_photo') }}
                        </strong>
                    </span>
                @endif
            </div>
        </div>

         <div class="form-group{{ $errors->has('app_form_type') ? ' has-error' : '' }}">
            <label class=" control-label" for="app_form_type">
                Pin Form
            </label>
            <div class="switch">
                <label>
                    <input {{ (isset($event) && $event->app_form_type=='pin')? 'checked="checked"':'' }}  type="checkbox" name="app_form_type" id="app_form_type">
                        <span class="lever switch-col-pink">
                        </span>
                    </input>
                </label>
            </div>
            @if ($errors->has('app_form_type'))
            <span class="help-block">
                <strong>
                    {{ $errors->first('app_form_type') }}
                </strong>
            </span>
            @endif
        </div>
            <div class="form-group{{ $errors->has('tag_image_option') ? ' has-error' : '' }}">
                <label class=" control-label" for="name">
                    Tag Image Option
                </label>
                <br />
                @php
                    $tag_image_options_logo = ((isset($event['tag_image_option']) && $event['tag_image_option'] == 'logo') || !isset($event['tag_image_option']) )? ' checked':'';
                    $tag_image_options_no_logo = ((isset($event['tag_image_option'])) && $event['tag_image_option'] == 'no_logo')? ' checked':'';
                    $tag_image_options_qr_code = ((isset($event['tag_image_option'])) && $event['tag_image_option'] == 'qr_code')? ' checked':'';
                @endphp
                <input class="with-gap" id="tag_image_options_logo" name="tag_image_option" type="radio" value="logo" {{$tag_image_options_logo}}/>
                <label for="tag_image_options_logo">
                    Logo
                </label>
                <input class="with-gap" id="tag_image_options_no_logo" name="tag_image_option" type="radio" value="no_logo" {{$tag_image_options_no_logo}} />
                <label class="m-l-20" for="tag_image_options_no_logo">
                    No Logo
                </label>
                <input class="with-gap" id="tag_image_options_qr_code" name="tag_image_option" type="radio" value="qr_code" {{$tag_image_options_qr_code}} />
                <label class="m-l-20" for="tag_image_options_qr_code">
                    QR code
                </label>
                <span class="help-block">
                <strong>
                    {{ $errors->first('tag_image_option') }}
                </strong>
            </span>
            </div>

    </div>
    <div class="col-md-8">
        <h4 class="card-inner-header">Form Input Fields</h4>
        {{--<div id="fb-editor" class="row"></div>--}}
        <input type="hidden" name="event_form_fields" value="">
        @php
            $formFields=['full_name'=>'Full Name','phone'=>'Phone','email'=>'Email','company'=>'Company Name','position'=>'Position','gender'=>'Gender','table_number'=>'Table Number','age'=>'Age','location'=>'Location','country'=>'Country','level_education'=>'Highest Level of Education','lived_abroad'=>'Lived Abroad (6 months above)','attending_as'=>'Attending As'];
        @endphp
        <ul class="list-inline" {{--style="display:none;"--}}>
        @foreach($formFields as $formKey =>$formLabel)
        <li>
        <div class="form-group{{ $errors->has('app_field_'.$formKey) ? ' has-error' : '' }} col-md-2">
            <label class=" control-label" for="app_field_{{ $formKey }}">
                {{ $formLabel }}
            </label>
            <div class="switch">
                <label>
                    @if(!in_array($formKey, ['full_name', 'phone', 'email', 'company', 'position']))
                    <input {{ (isset($event) && $event['app_field_'.$formKey]=='on')? 'checked="checked"':'' }} type="checkbox" name="app_field_{{ $formKey }}" id="app_field_{{ $formKey }}">
                    @else
                    <input {{ (isset($event) && $event['app_field_'.$formKey]=='off')? '':'checked="checked"' }} type="checkbox" name="app_field_{{ $formKey }}" id="app_field_{{ $formKey }}">
                    @endif
                        <span class="lever switch-col-pink">
                        </span>
                </label>
            </div>
            @if ($errors->has('app_field_'.$formKey))
            <span class="help-block">
                <strong>
                    {{ $errors->first('app_field_'.$formKey) }}
                </strong>
            </span>
            @endif
        </div>
        </li>
        @endforeach
        </ul>
        <h4 class="card-inner-header">Print Fields</h4>
        <ul class="list-inline">
            @foreach($printFields as $key => $printField)
                @php
                    $printFieldChecked = ((isset($printFieldRecord[$printField['key']])) && $printFieldRecord[$printField['key']] == 1)? ' checked':'';
                @endphp
                <li class="eagle-checkbox">
                    <label class="eagle-check custom-checkbox">
                        <input type="checkbox" name="print_fields[{{$printField['key']}}]" class="eagle-check-input" {{$printFieldChecked}} />
                        <span class="eagle-check-indicator"></span>
                        <span class="eagle-check-description">{{$printField['name']}}</span>
                    </label>
                </li>
            @endforeach
        </ul>

    </div>


    </div>
    <div class="tab-pane fade" id="sms-settings" role="tabpanel">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('sms_message') ? ' has-error' : '' }}">
                <label class=" control-label" for="event_sms_message">
                    SMS Message
                </label>
                <textarea  class="form-control" id="event_sms_message" name="sms_message" placeholder="Event SMS Message" rows="5">{{ old('sms_message',$event->sms_message ?? config('main.event_sms') ) }}</textarea>
                @if ($errors->has('excerpt'))
                <span class="help-block">
                    <strong>
                        {{ $errors->first('sms_message') }}
                    </strong>
                </span>
                @endif
            </div>

            <small>{!! __('Use <code>[EVENT_TITLE]</code> code as Event Title value, <code>[EVENT_LINK]</code> code as Event Page Link value and <code>[ATTENDEE_PIN_CODE]</code> code as Attendee PIN Code ') !!}</small>
             <small><strong>Hint:</strong> The square bracket blocks will be automatically replaced with the appropraite content when SMS is sent.</small>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <input class="btn btn-primary btn-lg pull-right" name="submit" type="submit" value="Submit"/>
        </div>
    </div>
</div>

@push('head')
<link rel="stylesheet" href="{{ asset('plugins/summernote/dist/summernote.css') }}"/>
<link rel="stylesheet" href="{{ asset('plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css') }}"/>
<link href="{{ asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}" rel="stylesheet">
    <style type="text/css">
        .form-wrap.form-builder .frmb {
            list-style-type: none;
            min-height: 200px !important;
            transition: background-color 500ms ease-in-out;
            border: 1px solid #00000029;
        }
    </style>
@endpush
@push('scripts')
<script src="{{ asset('plugins/summernote/dist/summernote.min.js')}}"></script>
<script src="{{ asset('plugins/boootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{ asset('js/form-builder.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.colorpicker').colorpicker();
    $('#event_details').summernote({
        height: 350, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false // set focus to editable area after initializing summernote
    });
    var today = new Date();
     today.setDate(today.getDate()-1);
     $('#start_date,#end_date').datepicker({
        startDate: today,
        todayHighlight:true,
    });

    let formBuilder =  $( document.getElementById('fb-editor')).formBuilder( {
        showActionButtons: false, // defaults: `true`
        disableFields: ['autocomplete', 'paragraph', 'hidden', 'file', 'header'],
        typeUserEvents: {
            text: {
                onadd: function(fld) {

                    let $patternField = $('.fld-pattern', fld);
                    $patternField.prop('disabled', true).parent().hide();
                    $('.fld-subtype', fld)
                        .change(function(e) {
                            console.log('fld-subtype change: ' + formBuilder.actions.getData());
                        });
                }
            }
        },
        defaultFields: [{
            className: "form-control",
            label: "First Name",
            placeholder: "Enter your first name",
            name: "field_first_name",
            required: true,
            type: "text"
        },{
            className: "form-control",
            label: "Phone",
            placeholder: "Enter Phone number",
            name: "field_phone",
            required: true,
            type: "text"
        },{
            className: "form-control",
            label: "Email",
            placeholder: "Enter your email",
            name: "field_email",
            required: false,
            type: "text"
        },{
            className: "form-control",
            label: "Company Name",
            placeholder: "Enter your company name",
            name: "field_company_name",
            required: false,
            type: "text"
        },{
            className: "form-control",
            label: "Position",
            placeholder: "Enter your position",
            name: "field_position",
            required: false,
            type: "text"
        }],
    });

    $("form").submit(function(){
        // Let's find the input to check
        let $input = $(this).find("input[name=event_form_fields]");
        if (!$input.val()) {
            // Value is falsely (i.e. null), lets set a new one
            $input.val(JSON.stringify(formBuilder.actions.getData()));
            console.log(formBuilder.actions.getData());
        }
    });
});

</script>
<script src="{{ asset('js/form-builder.min.js') }}"></script>

@endpush

