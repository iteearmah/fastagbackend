@extends('layouts.app')
@section('title'){{'Event - '.$event->title.' - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Event - [{{$event->title}}]
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
             <li>
                <a href="{{route('events.index')}}">
                    Events
                </a>
            </li>
            <li class="active">
                {{$event->title}}
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
              <ul class="nav nav-tabs nav-tabs-noborder nav-justified" role="tablist">
                <li role="presentation" class="active"><a href="#event-view" data-toggle="tab">VIEW</a></li>
                <li role="presentation"><a href="#event-attendees" data-toggle="tab">ATTENDANCE</a></li>
                  <li role="presentation"><a href="#event-guests" data-toggle="tab">GUESTS</a></li>
            </ul>
            <div class="card">
                <div class="body">
                    @include('flash::message')
                    <div class="tab-content">
                     <div class="tab-pane fade active in" id="event-view" role="tabpanel">
                        <div class="row">
                            <div class="clearfix" style="margin-bottom: 20px">
                                <ul class="list-inline">
                                    <li>
                                         <a class="btn btn-default" href="{{ route('events.edit',$event->id) }}" title="Edit this Event">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn btn-default" href="{{ route('attendees.index', ['event'=>$event->id]) }}" title="Event Attendees">
                                            <i class="fa fa-group"></i> Attendees
                                        </a>
                                    </li>
                                     <li>
                                         <a class="btn btn-default" href="{{ route('events.sms',$event->id) }}" title="Send SMS Brodcast">
                                            <i class="fa fa-mobile fa-lg"></i> SMS
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn btn-default" href="{{route('promopage',$event->uuid)}}" target="_blank" title="Goto Event Promo Page">
                                            <i class="fa fa-external-link"></i> Goto Promo Page
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn btn-default" href="{{ route('events.attendeesclockedin', ['event'=>$event->id]) }}" title="Refresh Attendess Count">
                                            <i class="fa fa-recycle"></i> Refresh App Count
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                 <div class="col-md-6">
                                <dl class="dl-vertical showview">
                                <dt>
                                    Attendee Total
                                </dt>
                                <dd>
                                    {{($event->attendee_event->count()) ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Event Code
                                </dt>
                                <dd>
                                    {{($event['event_code']) ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Organizer
                                </dt>
                                <dd>
                                    {{$event->Organizer->name ?? null}}
                                </dd>

                                <dt>
                                    Title
                                </dt>
                                <dd>
                                    {{$event->title}}
                                </dd>
                                <dt>
                                    Venue
                                </dt>
                                <dd>
                                    {{$event['venue'] ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Theme
                                </dt>
                                <dd>
                                    {{$event['theme'] ?? 'N/A'}}
                                </dd>
                                 <dt>
                                    Start Date
                                </dt>
                                <dd>
                                    {{($event->start_date)? $event->start_date->format('d M ,Y') : 'N/A'}}
                                </dd>

                            </dl>
                        </div>
                            </div>
                            <div class="col-md-6">
                                 <dl class="dl-vertical showview">
                                <dt>
                                    End Date
                                </dt>
                                <dd>
                                    {{($event->end_date)? $event->end_date->format('d M ,Y') : 'N/A'}}
                                </dd>
                                <dt>
                                    Excerpt
                                </dt>
                                <dd>
                                    {{$event['excerpt'] ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Status
                                </dt>
                                <dd>
                                    {{ucwords($event['status']) ?? 'N/A'}}
                                </dd>
                                <dt>
                                    Created
                                </dt>
                                <dd>
                                    {{$event->created_at->format('j, M Y')}}
                                </dd>
                                <dt>
                                    Logo
                                </dt>
                                <dd>
                                    @if(isset($event->cover_photo))
                                    <div class="fileinput-preview">
                                        <img alt="Logo" height="auto" id="photo_preview" class="thumbnail col-md-*" onerror="this.src='{{asset('photos/nophoto.jpg')}}'" src="{{ asset(config('main.cover_photo_dir').'/medium.'.$event->cover_photo )}}"/>
                                    </div>
                                    @endif
                                </dd>
                            </dl>
                            </div>
                            <div class="clearfix"></div>

                       {{--   <strong>
                            Details
                        </strong> <div id="event_details">
                            {!!$event['details'] !!}
                        </div>--}}

                    </div>
                    </div>
                   <div class="tab-pane fade" id="event-attendees" role="tabpanel">
                            <div class="row">
                                 <table class="table table-bordered table-striped table-hover event-attendees-table dataTable">
                                    @php
                                        $formFields=['full_name'=>'Full Name','phone'=>'Phone','email'=>'Email','company'=>'Company Name','position'=>'Position','gender'=>'Gender' ,'table_number'=>'Table Number','age'=>'Age','location'=>'Location','country'=>'Country','level_education'=>'Highest Level of Education','lived_abroad'=>'Lived Abroad (6 months above)','attending_as'=>'Attending As'];
                                    @endphp
                                    <thead>
                                            <tr>
                                                <th>ID</th>
                                                @foreach($formFields as $formKey =>$formLabel)
                                                    @if($event['app_field_'.$formKey]=='on')
                                                    <th>{{ $formLabel }}</th>
                                                    @endif
                                                @endforeach
                                                <th>Pin Code</th>
                                                @if($event_day_count > 0)
                                                    @for ($i = 1; $i <= $event_day_count; $i++)
                                                        <th>Day {{ $i }}</th>
                                                    @endfor
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($attendees)
                                            @foreach($attendees as $attendee)
                                            <tr>

                                                <td>
                                                    {{$attendee->id}}
                                                </td>
                                                @foreach($formFields as $formKey =>$formLabel)
                                                    @if($event['app_field_'.$formKey]=='on')
                                                    <td>{{ $attendee[$formKey] }}</td>
                                                    @endif
                                                @endforeach
                                                <td>
                                                    {{$attendee->attendee_event->pin_code}}
                                                </td>
                                                @if($event_day_count)

                                                    @foreach($event_days as $event_day => $value)
                                                         <td>
                                                            @if(isset($attendee_attendances[$attendee->id]) && isset($attendee_attendances[$attendee->id][$event_day]))
                                                                {{-- <i class="fa  fa-check-circle text-success"><span class="visible-print">✓</span></i> --}}
                                                                <span class="small" style="width: 63px; display: block;">{!!$attendee_attendances[$attendee->id][$event_day]!!}</span>
                                                           @else
                                                                <i class="fa fa-times-circle text-danger"><span class="visible-print">×</span></i>
                                                           @endif
                                                        </td>
                                                    @endforeach
                                                @endif
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                            </div>
                    </div>
                    <div class="tab-pane fade active in" id="event-guests" role="tabpanel">
                        <div class="row">
                            <table class="table table-bordered table-striped table-hover event-attendees-table dataTable">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Table Number</th>
                                        <th>No. of Seats on Table</th>
                                        <th>Type</th>
                                        <th>Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($event->event_guests)
                                    @foreach($event->event_guests as $eventguest)
                                    <tr>
                                        <td>{{$eventguest['name']}}</td>
                                        <td>{{$eventguest['table_number']}}</td>
                                        <td>{{$eventguest['seats_on_table']}}</td>
                                        <td>{{$eventguest['type']}}</td>
                                        <td>{{$eventguest->created_at->format('d M ,Y h:ia')}}</td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@push('head')
<link href="{{ asset('plugins/summernote/dist/summernote.css') }}" rel="stylesheet"/>
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
@endpush
@push('scripts')
<script src="{{ asset('plugins/summernote/dist/summernote.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{ asset('plugins//jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.keyTable.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/responsive.bootstrap.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.scroller.min.js')}}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.fixedHeader.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#event_details').summernote({
        height: 350, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false // set focus to editable area after initializing summernote
    });
    $('.event-attendees-table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});
</script>
@endpush
