@extends('layouts.app')
@section('title'){{'Events - Add New - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Events - Add New
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
             <li>
                <a href="{{route('events.index')}}">
                    Events
                </a>
            </li>
            <li class="active">
                Add New
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <form action="{{route('events.store')}}" class="" enctype="multipart/form-data" method="POST">
                            @include('events.form',['mainDataCollection'=>null])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
