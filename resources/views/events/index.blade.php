@extends('layouts.app')
@section('title'){{'Events - '.config('app.name')}}@endsection
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <h2>
            Events <a href="{{route('events.create')}}" class="btn btn-primary"><i class="fa fa-plus" ></i> Add New</a>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('dashboard')}}">
                    Dashboard
                </a>
            </li>
            <li class="active">
                Events
            </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                @if($organizer)
                    <div class="card-header">
                        <h2>
                            {{$organizer->name.' - Events'}}
                        </h2>
                    </div>
                @endif
                <div class="body">
                    @include('flash::message')
                    <div class="row">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Code
                                    </th>
                                    <th>
                                        Title
                                    </th>

                                    <th>
                                        Organizer
                                    </th>
                                    <th>
                                        No. of Attendees
                                    </th>
                                    <th>
                                        Start Date
                                    </th>
                                    <th colspan="5">

                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($events as $event)

                                <tr>
                                    <td>
                                        {{$event['event_code']}} {!! ($event['short_url']) ? '<a href="'. $event['short_url'] .'" title="'.$event['short_url'].'" target="_blank"><i class="fa fa-external-link fa-lg"></i></a>' : ''   !!}
                                    </td>
                                    <td>
                                        <a href="{{ route('events.edit',$event->id) }}"> {{$event['title']}}</a>
                                    </td>
                                    <td>
                                        <a href="{{route('organizers.show',$event->organizer)}}">{{$event->Organizer->name ?? null}}</a>
                                    </td>
                                    <td>
                                        {{ $event->attendee_event_count ?? 'N/A'}}
                                    </td>
                                    <td>
                                        {{($event->start_date)? $event->start_date->format('d M ,Y') : 'N/A'}}
                                    </td>


                                    <td>
                                        <a class="btn btn-default" href="{{ route('events.show',$event->id) }}" title="View">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-success" href="{{ route('attendees.index', ['event'=>$event->id]) }}" title="Attendees">
                                            <i class="fa fa-group"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn" href="{{ route('eventguest.index', ['event'=>$event->id]) }}" title="Show Event Guests">
                                            <i class="fa fa-user fa-lg"></i>
                                        </a>
                                    </td>
                                     <td>
                                        <a class="btn btn-info" href="{{ route('events.sms', ['event'=>$event->id]) }}" title="Send SMS">
                                            <i class="fa fa-mobile fa-lg"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('events.edit',$event->id) }}" title="Edit">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ route('events.destroy',['id'=>$event->id]) }}" method="post">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                                 <button class="btn btn-danger" type="submit" title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </input>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="col-md-12">
                            Showing {{($events->currentpage()-1)* $events->perpage()+1}} to {{(($events->currentpage()-1) * $events->perpage())+$events->count()}} of {{$events->total()}} entries
                        </div>
                        <div class="pull-right">{{ $events->links() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
